import ctypes
from pynq import MMIO

## Base address of sum_hw accelerator
XSUM_HW_BASE_ADDRESS  =  0x43c00000
XSUM_HW_END_ADDRESS   =  0x43cfffff
XSUM_HW_RANGE         =  XSUM_HW_END_ADDRESS - XSUM_HW_BASE_ADDRESS

## AXILiteS
## 0x00 : Control signals
##        bit 0  - ap_start (Read/Write/SC)
##        bit 1  - ap_done (Read/COR)
##        bit 2  - ap_idle (Read)
##        bit 3  - ap_ready (Read)
##        bit 7  - auto_restart (Read/Write)
##        others - reserved
## 0x04 : Global Interrupt Enable Register
##        bit 0  - Global Interrupt Enable (Read/Write)
##        others - reserved
## 0x08 : IP Interrupt Enable Register (Read/Write)
##        bit 0  - Channel 0 (ap_done)
##        others - reserved
## 0x0c : IP Interrupt Status Register (Read/TOW)
##        bit 0  - Channel 0 (ap_done)
##        others - reserved
## 0x10 : Data signal of ap_return
##        bit 31~0 - ap_return[31:0] (Read)
## 0x18 : Data signal of a
##        bit 31~0 - a[31:0] (Read/Write)
## 0x1c : reserved
## 0x20 : Data signal of b
##        bit 31~0 - b[31:0] (Read/Write)
## 0x24 : reserved
## (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)
XSUM_HW_AXILITES_ADDR_AP_CTRL   = 0x00
XSUM_HW_AXILITES_ADDR_GIE       = 0x04
XSUM_HW_AXILITES_ADDR_IER       = 0x08
XSUM_HW_AXILITES_ADDR_ISR       = 0x0c
XSUM_HW_AXILITES_ADDR_AP_RETURN = 0x10
XSUM_HW_AXILITES_BITS_AP_RETURN = 32
XSUM_HW_AXILITES_ADDR_A_DATA    = 0x18
XSUM_HW_AXILITES_BITS_A_DATA    = 32
XSUM_HW_AXILITES_ADDR_B_DATA    = 0x20
XSUM_HW_AXILITES_BITS_B_DATA    = 32

class SumHW:
    
    mmio = MMIO(XSUM_HW_BASE_ADDRESS, XSUM_HW_RANGE)
    
    def __init__(self):
        self.disable_auto_restart()
        
    
    def disable_auto_restart(self):
        self.mmio.write(XSUM_HW_AXILITES_ADDR_AP_CTRL, 0)
    
    
    def set_a(self, a):
        self.mmio.write(XSUM_HW_AXILITES_ADDR_A_DATA, a)
    
    
    def set_b(self, b):
        self.mmio.write(XSUM_HW_AXILITES_ADDR_B_DATA, b)
    
    
    def start(self):
        reg_val = self.mmio.read(XSUM_HW_AXILITES_ADDR_AP_CTRL) & 0x80
        self.mmio.write(XSUM_HW_AXILITES_ADDR_AP_CTRL, reg_val | 0x01)
    
    
    def read_result(self):
        
        while((self.mmio.read(XSUM_HW_AXILITES_ADDR_AP_CTRL) >> 1) & 0x1):
            pass
            
        ret_val = self.mmio.read(XSUM_HW_AXILITES_ADDR_AP_RETURN)
        return ctypes.c_long(ret_val & 0xFFFFFFFF).value
