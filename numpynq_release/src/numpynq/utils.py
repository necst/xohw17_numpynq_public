import os
from pynq import Overlay
from pynq import PL

def reconfigure_PL(new_ol_name):
    cur_ol_name = os.path.basename(PL.bitfile_name)
    
    if new_ol_name != cur_ol_name:
        ol = Overlay(new_ol_name)
        ol.download()
        return 1
    else:
        return 0
