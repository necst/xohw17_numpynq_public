# numPYNQ release
## Installation

* Make sure date is set on your PYNQ-Z1
* Download the numpynq\_release folder
* Move in numpynq\_release folder
* Run ```sudo python3.6 setup.py install```
* Run ```sudo cp ./libs/dma.h /usr/include```
* Run ```sudo cp ./libs/libdma.so /usr/lib```
* Open the python3.6 interpreter and you're good to go. Just run ```import numpynq as np```

