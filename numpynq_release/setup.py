#!/usr/bin/env python

import os
import shutil

from setuptools import setup, Extension

setup(
    name='numPYNQ',
    version='1.0',
    description='numPYNQ package',
    author='Riccardo Pressiani, Luca Stornaiuolo and Filippo Carloni',
    author_email='riccardo.pressiani@gmail.com',
    packages=['numpynq'],
    package_dir={'numpynq': 'src/numpynq'},
    install_requires=[
          'numpy',
      ],
    ext_modules = [
        Extension(
            '_DMA',
            libraries = ['dma', 'sds_lib'],
            library_dirs = ['/usr/lib'],
            sources = ['extensions/dma_module.c']
        ),
    ]
)

home    =   os.path.expanduser('~')

#   Copy bistreams in ~/pynq/bitstream
src     =   "./bitstreams"
dest    =   home + "/pynq/bitstream"
src_files = os.listdir(src)

for file_name in src_files:
    full_file_name = os.path.join(src, file_name)
    if (os.path.isfile(full_file_name)):
        shutil.copy(full_file_name, dest)

# src     =   "./libs"
# src_files = os.listdir(src)
# for file_name in src_files:
#     full_file_name = os.path.join(src, file_name)
#     if (os.path.isfile(full_file_name) and file_name == "libdma.so"):
#         shutil.copy(full_file_name, "/usr/lib")
#     if (os.path.isfile(full_file_name) and file_name == "dma.h"):
#         shutil.copy(full_file_name, "/usr/include")
