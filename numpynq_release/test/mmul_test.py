import pynq
import _DMA
import numpy
import time
import os


from cffi 	import FFI
from pynq 	import Overlay
from numpy 	import *
from pynq import Overlay
from pynq import PL
################################################################################
#
#	Reconfigure FPGA
#
################################################################################
def reconfigure_PL(new_ol_name):
    cur_ol_name = os.path.basename(PL.bitfile_name)
    
    if new_ol_name != cur_ol_name:
        ol = Overlay(new_ol_name)
        ol.download()
        return 1
    else:
        return 0

################################################################################
#
#	free_buf
# 
# 	Frees the pre-allocated local buffers
#
################################################################################
def free_buf():
	#	Free buffer allocated spaces
	dma0in._free_buf(ffi.cast("uint32_t", _buff_dma0out))
	dma0out._free_buf(ffi.cast("uint32_t", _buff_dma0in))
	dma1in._free_buf(ffi.cast("uint32_t", _buff_dma1in))

################################################################################
#
#	_dot_general
# 
# 	Implements the hardware accelerated version of the dot product
# 	between matrices of whatever dimensions
#
################################################################################
def _dot_general(a, b):
	#	Download general dot product bitstream on FPGA module
	# 	if it is not already loaded
	reconfigure_PL("mmult_stream.bit")

	#	Cast buffer to matrix number type
	buff_dma1in 	= ffi.cast("unsigned int *", _buff_dma1in)
	buff_dma0in 	= ffi.cast("unsigned int *", _buff_dma0in)
	buff_dma0out 	= ffi.cast("unsigned int *", _buff_dma0out)

	# 	Get physical DMA addresses
	buff_dma0in_phy		= dma0in._get_phy_addr()
	buff_dma1in_phy		= dma1in._get_phy_addr()
	buff_dma0out_phy	= dma0out._get_phy_addr()

	# 	Get matrices dimensions
	a_rows = a.shape[0]
	b_cols = a.shape[1]
	common_rows_cols = a.shape[1]

	################################################################################
	#
	#	Initiate transfer out
	#	param1: Size of the result in bytes
	#	param2: Physical addr of the output buffer
	#	param3: Transfer direction (1 stands for output)
	#
	################################################################################

	dma0out._transfer(a_rows * b_cols * 4, buff_dma0out_phy, 1)

	#	Send to the DMA the number of iterations
	buff_dma0in[0] = a_rows * b_cols
	dma0in._transfer(4, buff_dma0in_phy, 0)
	dma0in._wait(0)

	#	Send to the DMA the number of elements to be multiplied in an iteration
	buff_dma1in[0] = common_rows_cols
	dma1in._transfer(4,buff_dma1in_phy,0)
	dma1in._wait(0)

	#	Convert the matrix into bytes representation and transpose the second matrix
	matrix_a_bytes = a.tobytes();
	matrix_b_bytes = b.transpose().tobytes();

	#	Get the matrix size
	matrix_a_size = a_rows * common_rows_cols * 4
	matrix_b_size = b_cols * common_rows_cols * 4

	#	Move the content of the NumPy generated matrix to the DMA buffers
	ffi.memmove(buff_dma0in, matrix_a_bytes, matrix_a_size)
	ffi.memmove(buff_dma1in, matrix_b_bytes, matrix_b_size)

	#	Dedicated transfer function for matrix multiply
	dma0in._opt_transfer(
		dma1in._get_DMA_engine(),
		buff_dma0in_phy,
		buff_dma1in_phy,
		common_rows_cols * 4,
		a_rows,
		b_cols,
		0
	)
	dma0out._wait(1)


	#	Move buffer content to NumPy data structure
	buffer_size = a_rows*b_cols*4
	c_buffer = ffi.buffer(buff_dma0out,buffer_size)
	np_arr2 = frombuffer(c_buffer, dtype=int)
	result_HW = reshape(np_arr2,(a_rows, b_cols))

	return result_HW

################################################################################
#
#	_dot_84
# 
# 	Implements the hardware accelerated version of the dot product
# 	between 84x84 matrices
#
################################################################################
def _dot_84(a, b):
	#	Download 84x84 dot product bitstream on FPGA module
	# 	if it is not already loaded
	reconfigure_PL("matr_mul_84.bit")

	DIM=84

	# 	Get matrices dimensions
	a_rows = a.shape[0]
	b_cols = a.shape[1]

	# 	Pad matrices if smaller than 84x84
	if a.shape[0] < DIM or a.shape[1] < DIM:
		pad_rows = DIM - a.shape[0]
		pad_cols = DIM - a.shape[1]
		a = numpy.lib.pad(a, ((0,pad_rows),(0,pad_cols)), 'constant', constant_values=(0))

	if b.shape[0] < DIM or b.shape[1] < DIM:
		pad_rows = DIM - b.shape[0]
		pad_cols = DIM - b.shape[1]
		b = numpy.lib.pad(b, ((0,pad_rows),(0,pad_cols)), 'constant', constant_values=(0))

	#	Cast buffer to matrix number type
	buff_dma0in 	= ffi.cast("unsigned int *", _buff_dma0in)
	buff_dma0out 	= ffi.cast("unsigned int *", _buff_dma0out)

	# 	Get physical DMA addresses
	buff_dma0in_phy         = dma0in._get_phy_addr()
	buff_dma0out_phy        = dma0out._get_phy_addr()

	matrix_a_size = DIM * DIM * 4
	matrix_b_size = DIM * DIM * 4

	################################################################################
	#
	#	Initiate transfer out
	#	param1: Size of the result in bytes
	#	param2: Physical addr of the output buffer
	#	param3: Transfer direction (1 stands for output)
	#
	################################################################################
	dma0out._transfer(DIM*DIM*4,buff_dma0out_phy,1)

	#	Move the content of the NumPy generated matrix to the DMA buffers
	ffi.memmove(buff_dma0in, matrix_a, matrix_a_size)
	dma0in._transfer(DIM*DIM*4,buff_dma0in_phy,0)
	dma0in._wait(0)

	ffi.memmove(buff_dma0in, matrix_b, matrix_b_size)
	dma0in._transfer(DIM*DIM*4,buff_dma0in_phy,0)
	dma0in._wait(0)

	#	Wait for result
	dma0out._wait(1)

	#	Move buffer content to NumPy data structure
	buffer_size = DIM*DIM*4
	c_buffer = ffi.buffer(buff_dma0out,buffer_size)
	np_arr = frombuffer(c_buffer, dtype=int)
	result_HW = reshape(np_arr,(DIM, DIM))

	return result_HW[:a_rows,:b_cols]

################################################################################
#
#	dot
# 
# 	Checks the signals passed as parameters and implements a runtime analysis
# 	to execute the most performing option for the matrix dot product
#
################################################################################
def dot(a, b):

	# 	Break even dimension for which it is more convinient to 
	# 	execute the dot product with the FPGA rather than the CPU
	BE_DIM = 700*700

	# 	If not matrices of integers -> execute operation via NumPy
	if a.dtype != numpy.dtype('int') or b.dtype != numpy.dtype('int'):
		return numpy.dot(a,b)

	# 	Raise exception if dot product is not possible due to wrong
	# 	matrices dimensions
	if a.shape[1] != b.shape[0]:
		raise ValueError("Wrong matrices dimensions")

	# 	Runtime analysis of the size of the matrices in order to
	# 	execute the elaboration with the most performing option
	if a.shape[0]*a.shape[1]+b.shape[0]*b.shape[1] >= BE_DIM+BE_DIM:
		return _dot_general(a,b)
	elif a.shape[0] <= 84 and a.shape[1] <= 84 and b.shape[1] <= 84:	
		return _dot_84(a,b)
	else:
		return numpy.dot(a,b)

#	Download correlation bitstream on FPGA module
#	So that it is possible to instantiate DMAs objects
Overlay("correlation.bit").download()

#	Initialize FFI object
ffi = FFI()

################################################################################
#
#	DMA initialization
#	0 stands for input direction
#	1 stands for output direction
#	3 stands for bidirectional
#
################################################################################
dma0in 	= _DMA.dma(0x40400000, 0)
dma0out = _DMA.dma(0x40400000, 1)
dma1in 	= _DMA.dma(0x40410000, 0)

################################################################################
#
#	Get buffer address
#	buff_dmax_phy is the physical address of the DMA associated buffer
#	buff_dmax is the virtual address of the DMA associated buffer
#
################################################################################
_buff_dma0in 	=	dma0in._create_buf(1050 * 1050 * 4, 0)
_buff_dma1in 	=	dma1in._create_buf(1050 * 1050 * 4, 0)
_buff_dma0out 	=	dma0out._create_buf(1050 * 1050 * 4, 0)

MATRIX_DIM = 1024

x = numpy.random.randint(50.0, size=(MATRIX_DIM, MATRIX_DIM))
y = numpy.random.randint(50.0, size=(MATRIX_DIM, MATRIX_DIM))

start_hw = time.clock()
result_HW = dot(x,y)
end_hw = time.clock()
time_hw = end_hw-start_hw
print("EXEC TIME HW: ", time_hw)

start_sw = time.clock()
result_SW = numpy.dot(x,y)
end_sw = time.clock()
time_sw = end_sw-start_sw
print("EXEC TIME SW: ", time_sw)

speedup = time_sw/time_hw
print(" ")
print("SPEEDUP", speedup)

free_buf()
