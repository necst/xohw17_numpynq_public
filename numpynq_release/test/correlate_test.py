import pynq
import _DMA
import numpy
import time
import os


from cffi 	import FFI
from pynq 	import Overlay
from numpy 	import *
from pynq import Overlay
from pynq import PL
################################################################################
#
#	Reconfigure FPGA
#
################################################################################
def reconfigure_PL(new_ol_name):
    cur_ol_name = os.path.basename(PL.bitfile_name)
    
    if new_ol_name != cur_ol_name:
        ol = Overlay(new_ol_name)
        ol.download()
        return 1
    else:
        return 0

################################################################################
#
#	free_buf
# 
# 	Frees the pre-allocated local buffers
#
################################################################################
def free_buf():
	#	Free buffer allocated spaces
	dma0in._free_buf(ffi.cast("uint32_t", _buff_dma0out))
	dma0out._free_buf(ffi.cast("uint32_t", _buff_dma0in))
	dma1in._free_buf(ffi.cast("uint32_t", _buff_dma1in))

################################################################################
#
#	correlate
# 
# 	Implements the hardware accelerated version of the correlate function
#
################################################################################
def correlate(x, y, mode='valid'):
	
	# 	Check if the the correlation requested is mode="full",
	# 	otherwise execute NumPy correlate
	if mode != "full":
		return numpy.correlate(x,y,mode)

	#	Download correlation bitstream on FPGA module
	# 	if it is not already loaded
	reconfigure_PL("correlation.bit")

	MAX_DMAs_BUFFER_DIMENSION = 1050 * 1050
	LAGS_PER_ITERATION = 80

	#	Cast buffer to matrix number type
	buff_dma0in     = ffi.cast("float *", _buff_dma0in)
	buff_dma1in     = ffi.cast("float *", _buff_dma1in)
	buff_dma0out    = ffi.cast("float *", _buff_dma0out)

	# 	Get physical DMA addresses
	buff_ptr_padded_x   = dma0in._get_phy_addr()
	buff_ptr_padded_y   = dma1in._get_phy_addr()
	buff_ptr_buff_out   = dma0out._get_phy_addr()

	#   Compute number of points of the longest array
	num_points = max(len(x), len(y))

	# 	Pad signals in case of different lengths
	if len(x) < len(y):
		len_diff = len(y)-len(x)
		x = numpy.lib.pad(x, (0,len_diff), 'constant', constant_values=(0))
	elif len(x) > len(y):
		len_diff = len(x)-len(y)
		y = numpy.lib.pad(y, (0,len_diff), 'constant', constant_values=(0))

	# Convert data type to float32
	x = x.astype(float32)
	y = y.astype(float32)

	# variables for correlation purpose
	padded_lag_max = (num_points + LAGS_PER_ITERATION - (num_points + LAGS_PER_ITERATION) % LAGS_PER_ITERATION)
	n_iterations = int(padded_lag_max / LAGS_PER_ITERATION)
	number_of_pads_at_begin = LAGS_PER_ITERATION - 1
	max_number_of_pads_at_end = (LAGS_PER_ITERATION * (n_iterations)) - 1
	size_of_padded_signal = number_of_pads_at_begin + num_points + max_number_of_pads_at_end

	if size_of_padded_signal > MAX_DMAs_BUFFER_DIMENSION:
		raise ValueError("ERROR: buffers too small to contain signals")

	#	Set num_points and lag_max at the beginning of the buff
	buff_dma0in[0] = float32(num_points)
	buff_dma1in[0] = float32(num_points)

	#	Move the content of the NumPy generated matrix to the DMA buffers
	ffi.memmove(buff_dma0in + 1 + number_of_pads_at_begin, x, num_points * 4)
	ffi.memmove(buff_dma1in + 1 + number_of_pads_at_begin, y, num_points * 4)

	#	Dedicated correlate function for matrix multiply
	dma0in._correlate(
		dma1in._get_DMA_engine(),
		dma0out._get_DMA_engine(),
		buff_ptr_buff_out,
		buff_ptr_padded_x,
		buff_ptr_padded_y,
		num_points,
		num_points,
		LAGS_PER_ITERATION
	)

	#	Move buffer content to NumPy data structure
	buffer_size = num_points * 4
	c_buffer = ffi.buffer(buff_dma0out, buffer_size)
	result_HW = frombuffer(c_buffer, dtype=float32)

	# Reverse array
	result_HW_1 = numpy.copy(result_HW)[::-1]

	#	Dedicated correlate function for matrix multiply
	dma0in._correlate(
		dma1in._get_DMA_engine(),
		dma0out._get_DMA_engine(),
		buff_ptr_buff_out,
		buff_ptr_padded_y,
		buff_ptr_padded_x,
		num_points,
		num_points,
		LAGS_PER_ITERATION
	)

	#	Move buffer content to NumPy data structure
	buffer_size = num_points * 4
	c_buffer = ffi.buffer(buff_dma0out, buffer_size)
	result_HW = frombuffer(c_buffer, dtype=float32)
	
	# 	Remove first element
	result_HW_2 = numpy.delete(numpy.copy(result_HW), 0, 0)
	
	#	Concatenate results
	result_HW = numpy.append(result_HW_1,result_HW_2)

	if len(x) < len(y):
		return result_HW[len_diff:]
	elif len(x) > len(y):
		return result_HW[:len_diff]
	else:
		return result_HW

#	Download correlation bitstream on FPGA module
#	So that it is possible to instantiate DMAs objects
Overlay("correlation.bit").download()

#	Initialize FFI object
ffi = FFI()

################################################################################
#
#	DMA initialization
#	0 stands for input direction
#	1 stands for output direction
#	3 stands for bidirectional
#
################################################################################
dma0in 	= _DMA.dma(0x40400000, 0)
dma0out = _DMA.dma(0x40400000, 1)
dma1in 	= _DMA.dma(0x40410000, 0)

################################################################################
#
#	Get buffer address
#	buff_dmax_phy is the physical address of the DMA associated buffer
#	buff_dmax is the virtual address of the DMA associated buffer
#
################################################################################
_buff_dma0in 	=	dma0in._create_buf(1050 * 1050 * 4, 0)
_buff_dma1in 	=	dma1in._create_buf(1050 * 1050 * 4, 0)
_buff_dma0out 	=	dma0out._create_buf(1050 * 1050 * 4, 0)

SIGNAL_LENGTH = 75000

x = numpy.random.uniform(50.0, size=(SIGNAL_LENGTH))
y = numpy.random.uniform(50.0, size=(SIGNAL_LENGTH))

start_hw = time.clock()
result_HW = correlate(x,y,mode="full")
end_hw = time.clock()
time_hw = end_hw-start_hw
print("EXEC TIME HW: ", time_hw)

start_sw = time.clock()
result_SW = numpy.correlate(x,y,mode="full")
end_sw = time.clock()
time_sw = end_sw-start_sw
print("EXEC TIME SW: ", time_sw)

speedup = time_sw/time_hw
print(" ")
print("SPEEDUP", speedup)

errors = numpy.absolute(result_SW - result_HW)

mape = numpy.mean(numpy.abs((result_SW - result_HW) / result_SW)) * 100

print("Mean Absolute Percentage Error: ", mape)

free_buf()
