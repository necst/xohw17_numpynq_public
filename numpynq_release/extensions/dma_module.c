#include "/opt/python3.6/include/python3.6m/Python.h"
#include <structmember.h>
#include <dma.h>
#include <stdio.h>

#define DMA_TO_DEV          0
#define DMA_FROM_DEV        1
#define DMA_BIDIRECTIONAL   3

typedef struct{
   PyObject_HEAD;
   uint32_t direction;
   uint32_t  phyAddress;
   uint32_t TransferInitiated;
   XAxiDma  DMAengine;
   XAxiDma_Config  DMAinstance;
   uint32_t* buf;
}  _dma;

uint32_t cma_mmap(uint32_t phyAddr, uint32_t len);
uint32_t cma_munmap(void *buf, uint32_t len);
void *cma_alloc(uint32_t len, uint32_t cacheable);
uint32_t cma_get_phy_addr(void *buf);
void cma_free(void *buf);


static void _dma_dealloc(_dma* self){
      Py_TYPE(self)->tp_free((PyObject*)self);
}

static PyObject* _dma_new(PyTypeObject* type, PyObject *args, PyObject* kwds){
      _dma* self;
      self=(_dma *)type->tp_alloc(type,0);
      if(self!= NULL){

      }
      return (PyObject *)self;
}

void _gen_config(_dma* self, uint32_t address, uint32_t direction){
      self->DMAinstance.DeviceId=1;
      self->DMAinstance.BaseAddr=(uint32_t *)cma_mmap(address,0x10000);
      self->DMAinstance.HasStsCntrlStrm = 0;

      self->DMAinstance.HasMm2SDRE = 0;
      self->DMAinstance.HasS2MmDRE = 0;
      self->DMAinstance.Mm2sNumChannels = 1;
      self->DMAinstance.S2MmNumChannels = 1;
      self->DMAinstance.Mm2SBurstSize = 16;
      self->DMAinstance.S2MmBurstSize = 64;
      self->DMAinstance.MicroDmaMode = 0;
      self->DMAinstance.AddrWidth = 32;
      self->DMAinstance.Mm2SDataWidth = 32;
      self->DMAinstance.S2MmDataWidth = 64;

      if (direction == DMA_TO_DEV)
      {
        self->DMAinstance.HasS2Mm = 0;
        self->DMAinstance.HasMm2S = 1;
      } else if (direction == DMA_BIDIRECTIONAL)
      {
        self->DMAinstance.HasS2Mm = 1;
        self->DMAinstance.HasMm2S = 1;
      } else {
        self->DMAinstance.HasS2Mm = 1;
        self->DMAinstance.HasMm2S = 0;
      }

      self->TransferInitiated = 0;
}

void mult_transfer(XAxiDma *InstancePtrA, XAxiDma *InstancePtrB, uint32_t * BuffAddrA, uint32_t * BuffAddrB,
  uint32_t CommonBytes, uint32_t Rows, uint32_t Columns, int Direction) {
  uint32_t common_rows_cols = CommonBytes/4;

  for(uint32_t i = 0; i < Rows; i++) {
    for (uint32_t j = 0; j < Columns; j++)
    {
      XAxiDma_SimpleTransfer(InstancePtrA, BuffAddrA+(common_rows_cols*(i)), CommonBytes, Direction);
      XAxiDma_SimpleTransfer(InstancePtrB, BuffAddrB+(common_rows_cols*(j)), CommonBytes, Direction);
      while(1) {
        if (XAxiDma_Busy(InstancePtrA, Direction) == 0 && XAxiDma_Busy(InstancePtrB, Direction) == 0) break;
      }
    }

  }
}

static PyObject *_opt_transfer(_dma *self, PyObject *args) {
    uint32_t bufPtr1, bufPtr2, commonBytes, rows, columns, direction;
    long DMAengine2;

    if (! PyArg_ParseTuple(args, "l|I|I|I|I|I|I", &DMAengine2, &bufPtr1, &bufPtr2, &commonBytes, &rows, &columns, &direction))
        return NULL;

    self->direction = direction;
    mult_transfer(&self->DMAengine, (XAxiDma *)DMAengine2, (uint32_t*)bufPtr1, (uint32_t*)bufPtr2, commonBytes, rows, columns, direction);
    self->TransferInitiated = 1;

    Py_RETURN_NONE;
}

static int _dma_init(_dma* self,PyObject *args){
          self->direction = DMA_FROM_DEV;
          if(!PyArg_ParseTuple(args,"I|I",&self->phyAddress, &self->direction))
            return -1;
          _gen_config(self, self->phyAddress, self->direction);
          self->DMAengine.RegBase = 99;
          
          if(XAxiDma_CfgInitialize(&self->DMAengine, &self->DMAinstance)){
            return -1;
          }

          XAxiDma_Reset(&(self->DMAengine));
          DisableInterruptsAll(&(self->DMAengine));
          return 0;
}

static PyObject *_get_DMA_engine(_dma *self, PyObject *args) {
  return PyLong_FromVoidPtr((void*)&self->DMAengine);
}

static PyObject *_test(_dma *self, PyObject *args) {
  uint32_t num;
  int direction;
  if (! PyArg_ParseTuple(args, "I|I", &num,&direction))
      return NULL;
  printf("IT'S SOMETHING, %d!\n",self->direction);
  Py_RETURN_NONE;
}

static PyObject *_wait(_dma *self,PyObject *args) {
int direction;
   PyArg_ParseTuple(args,"I",&direction);
  if (self->TransferInitiated == 0)
  {
    return Py_BuildValue("I", 0);
  }
  while(1) {
    if (XAxiDma_Busy(&self->DMAengine, direction) == 0) break;
  }
  return Py_BuildValue("I", 1);
}

static PyObject *_transfer(_dma *self, PyObject *args) {
    uint32_t num_bytes, _bufPtr, direction;

    if (! PyArg_ParseTuple(args, "I|I|I", &num_bytes, &_bufPtr, &direction))
        return NULL;

    uint32_t* buf_ptr_cast = (uint32_t *)_bufPtr;
    self->direction = direction;
    XAxiDma_SimpleTransfer(&self->DMAengine, buf_ptr_cast, num_bytes, direction);
    self->TransferInitiated = 1;

    Py_RETURN_NONE;
}

static PyObject *_create_buf(_dma *self, PyObject *args) {
    uint32_t num_bytes, cacheable;

    if (! PyArg_ParseTuple(args, "I|I", &num_bytes, &cacheable)) {
      return NULL;
    }

    self->buf = cma_alloc(num_bytes, cacheable);

    if(self->buf == NULL) {
      return PyTuple_Pack(2, Py_BuildValue("I", -1), Py_BuildValue("I", -1));
    }

    return PyLong_FromVoidPtr((void*)self->buf);
}

static PyObject *_get_phy_addr(_dma *self) {
    uint32_t *_bufPtr = (uint32_t *)cma_get_phy_addr(self->buf);
    return PyLong_FromVoidPtr((void*)_bufPtr);
}

static PyObject *_free_buf(_dma *self, PyObject *args) {
    int buf;

    if (! PyArg_ParseTuple(args, "I", &buf))
        return NULL;

    if((uint32_t *)buf == NULL){
      return Py_BuildValue("I", -1);
    } 
        
    cma_free((uint32_t *)buf);
    
    return Py_BuildValue("I", 1);
}

/* Function to reverse array from start to end*/
void reverse_array(float *arr, int start, int end)
{
    float temp;
    while (start < end)
    {
        temp = arr[start];
        arr[start] = arr[end];
        arr[end] = temp;
        start++;
        end--;
    }
}

static PyObject *_correlate(_dma *self, PyObject *args){
    float *buff_out,*padded_signal_x,*padded_signal_y;  
    int num_points;
    long DMAengine2, DMAengine3;
    int LAGS_PER_ITERATION;
    int lag_max;
    if (! PyArg_ParseTuple(args, "l|l|l|l|l|I|I|I",&DMAengine2,&DMAengine3,&buff_out,&padded_signal_x,&padded_signal_y,&num_points,&lag_max,&LAGS_PER_ITERATION))
        return NULL;
  //dma0 is self, dma1  engine2
   
    // ========== LOCAL VARIABLES ==========
  int i;

    int padded_lag_max = (lag_max + LAGS_PER_ITERATION - (lag_max + LAGS_PER_ITERATION) % LAGS_PER_ITERATION);
    int n_iterations = (padded_lag_max / LAGS_PER_ITERATION);
    int points_per_iteration = num_points + LAGS_PER_ITERATION - 1;

    // =========== HOW TO CREATE STREAMS ===========
    /*
     * ITERATION 0)
     * stream_1 : [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION - 1) + signal[0, num_points)
     * stream_2 : signal[0, num_points) + [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION - 1)
     *
     * ITERATION i)
     * stream_1 : [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION - 1) + signal[0, num_points)
     * stream_2 : signal[LAGS_PER_ITERATION * i, num_points) + [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION * i) + [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION - 1)
     */

    // Create a padded signal from the original one (calloc set allocated buffer to 0).
    int number_of_pads_at_begin = LAGS_PER_ITERATION - 1;

    // @@@@@@@@@@ HW EXECUTION @@@@@@@@@@@@@@@
    // ========== SEND PARAMETERS ==========
    // Push each parameter at the beginning of the streams and wait until they are received by the core.
    XAxiDma_SimpleTransfer(&self->DMAengine, (uint32_t * ) padded_signal_x, sizeof(float),
    DMA_TO_DEV);
    while (XAxiDma_Busy(&self->DMAengine, DMA_TO_DEV))
        ;
    
    XAxiDma_SimpleTransfer((XAxiDma *)DMAengine2, (uint32_t * ) padded_signal_y, sizeof(float),
    DMA_TO_DEV);
    while (XAxiDma_Busy((XAxiDma *)DMAengine2, DMA_TO_DEV))
        ;
    
    // Now all parameters are sent by processor and received by the core.

    // ========== OPEN STREAM TO READ RESULTS ==========
    // Open stream to receive data from the core and start read as soon as the data are available
    XAxiDma_SimpleTransfer((XAxiDma *)DMAengine3, (uint32_t *) buff_out, padded_lag_max * 4,
    DMA_FROM_DEV);

    // ========== SEND SIGNAL ==========
    // Send signal to the core per iteration.
    for (i = 0; i < n_iterations; i++) {

      XAxiDma_SimpleTransfer(
        &self->DMAengine,
        (uint32_t *) padded_signal_x + 1,
        points_per_iteration * 4,
        DMA_TO_DEV
      );

      XAxiDma_SimpleTransfer((XAxiDma *)DMAengine2,
          (uint32_t *) padded_signal_y + 1 + number_of_pads_at_begin + LAGS_PER_ITERATION * i,
              points_per_iteration * 4,
          DMA_TO_DEV);

      // Wait until data of this transfer are received.
      while (XAxiDma_Busy(&self->DMAengine, DMA_TO_DEV)
          || XAxiDma_Busy((XAxiDma *)DMAengine2, DMA_TO_DEV))
        ;
    }

  // ========== WAIT RESULTS ==========
  // Wait until all data of output are read from the core.
  while (XAxiDma_Busy(&self->DMAengine, DMA_FROM_DEV))
    ;

    Py_RETURN_NONE;
}

// exposing members 
static PyMemberDef _DMA_dma_members[] = {
    {"direction", T_UINT, offsetof(_dma, direction), 0,
     "transfer direction"},
    {"phyAddress", T_UINT, offsetof(_dma, phyAddress), 0,
     "physical address"},
    {"TransferInitiated", T_UINT, offsetof(_dma, TransferInitiated), 0,
     "TransferInitiated flag"},
    {"DMAengine", T_UINT, offsetof(_dma, DMAengine), 0,
     "DMAengine address"},
    {"DMAinstance", T_UINT, offsetof(_dma, DMAinstance), 0,
     "DMAinstance"},
    {NULL}  /* Sentinel */
};

/*
*   defining the methods
*/
static PyMethodDef _dma_methods[] = {
    {"_get_DMA_engine", (PyCFunction)_get_DMA_engine, METH_VARARGS,
     "get DMA engine addr"},
    {"_opt_transfer", (PyCFunction)_opt_transfer, METH_VARARGS,
     "start DMA transfer for mul"},
    {"_test", (PyCFunction)_test, METH_VARARGS,
     "test"},
    {"_wait", (PyCFunction)_wait, METH_VARARGS,
     "DMA wait"},
    {"_transfer", (PyCFunction)_transfer, METH_VARARGS,
     "DMA transfer"},
    {"_create_buf", (PyCFunction)_create_buf, METH_VARARGS,
     "DMA create buf"},
    {"_free_buf", (PyCFunction)_free_buf, METH_VARARGS,
     "DMA free buffer"},
    {"_get_phy_addr", (PyCFunction)_get_phy_addr, METH_VARARGS,
     "DMA phy addr"},
    {"_correlate", (PyCFunction)_correlate, METH_VARARGS,
     "DMA free buffer"},
    
    {NULL}  /* Sentinel */
};

static PyTypeObject _DMA_dmaType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "_DMA.dma",                                 /* tp_name */
    sizeof(_dma),                               /* tp_basicsize */
    0,                                          /* tp_itemsize */
    (destructor)_dma_dealloc,                   /* tp_dealloc */
    0,                                          /* tp_print */
    0,                                          /* tp_getattr */
    0,                                          /* tp_setattr */
    0,                                          /* tp_reserved */
    0,                                          /* tp_repr */
    0,                                          /* tp_as_number */
    0,                                          /* tp_as_sequence */
    0,                                          /* tp_as_mapping */
    0,                                          /* tp_hash  */
    0,                                          /* tp_call */
    0,                                          /* tp_str */
    0,                                          /* tp_getattro */
    0,                                          /* tp_setattro */
    0,                                          /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,   /* tp_flags */
    "DMA objects",
    0,                                          /* tp_traverse */
    0,                                          /* tp_clear */
    0,                                          /* tp_richcompare */
    0,                                          /* tp_weaklistoffset */
    0,                                          /* tp_iter */
    0,                                          /* tp_iternext */
    _dma_methods,                               /* tp_methods */
    _DMA_dma_members,                           /* tp_members */
    0,                                          /* tp_getset */
    0,                                          /* tp_base */
    0,                                          /* tp_dict */
    0,                                          /* tp_descr_get */
    0,                                          /* tp_descr_set */
    0,                                          /* tp_dictoffset */
    (initproc)_dma_init,                        /* tp_init */
    0,                                          /* tp_alloc */
    _dma_new,                                   /* tp_new */
};
static PyModuleDef _DMAmodule = {
    PyModuleDef_HEAD_INIT,
    "_DMA",
    "Helper module for pyxi. It implements C bindings towards the PL.",
    -1,
    NULL, NULL, NULL, NULL, NULL
};

PyMODINIT_FUNC PyInit__DMA(void){
    PyObject* m;

    if (PyType_Ready(&_DMA_dmaType) < 0)
        return NULL;

    m = PyModule_Create(&_DMAmodule);
    if (m == NULL)
        return NULL;

    Py_INCREF(&_DMA_dmaType);
    PyModule_AddObject(m, "dma", (PyObject *)&_DMA_dmaType); // importing the mmio object
    return m;
}
