#include <hls_stream.h>
#include <stdio.h>

#define N_ROWS_A 6
#define COMMON_ROWS_COLS 9
#define N_COLS_B 2

struct data_struct{
  float data;
  bool last;
};

void matr_mul_stream(hls::stream<float> &x, hls::stream<float> &y, hls::stream<data_struct> &out);

void matrix_mul_sw(float A[N_ROWS_A][COMMON_ROWS_COLS], float B[COMMON_ROWS_COLS][N_COLS_B], float C[N_ROWS_A][N_COLS_B]){

    for (int i = 0; i < N_ROWS_A; i++) {
        for (int j = 0; j < N_COLS_B; j++) {
            for (int k = 0; k < COMMON_ROWS_COLS; k++) {
                C[i][j] += A[i][k] * B[k][j];
            }
        }
    }

}

void print_matrix_mul(float A[N_ROWS_A][COMMON_ROWS_COLS], float B[COMMON_ROWS_COLS][N_COLS_B]){

    for (int i = 0; i < N_ROWS_A; i++) {
        for (int j = 0; j < N_COLS_B; j++) {
            if (j != 0)
                printf("\t|\t");
            for (int k = 0; k < COMMON_ROWS_COLS; k++) {
                if (k != 0)
                    printf("  +  ");
                printf("%f * %f", A[i][k], B[k][j]);
            }
        }
        printf("\n");
    }
    printf("\n");

}

int main(){

    float A[N_ROWS_A][COMMON_ROWS_COLS] = {
        {1,2,3,0,2,4.2,5,6,3},
        {4,5,6.4,1,2,9,8,6,4},
        {0,2,3,2,3,3,2,0,8},
        {9,8,1,4.5,5,6,1,2,9.1},
        {4,5,6,1,2,9,8,6,4},
        {9,8,1,4,5,6,1,2.0,9}
    };

    float B[COMMON_ROWS_COLS][N_COLS_B] = {
        {4,1},
        {0,5.3},
        {3,2},
        {3.7,2},
        {3,2},
        {3,2.3},
        {0,5},
        {0,5},
        {0.5,5}
    };

    print_matrix_mul(A, B);

    float C[N_ROWS_A][N_COLS_B] = {
            {0,0},
            {0,0},
            {0,0},
            {0,0},
            {0,0},
            {0,0}
    };

    printf("\n====================\nSW MATRIX MUL:\n====================\n");
    matrix_mul_sw(A, B, C);

    // PRINT RESULT
    for (int i = 0; i < N_ROWS_A; i++) {
        for (int j = 0; j < N_COLS_B; j++) {
            if (j != 0)
                printf("\t|\t");
            printf("%f", C[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    printf("\n====================\nHW MATRIX MUL:\n====================\n");

    hls::stream<float> a("a");
    hls::stream<float> b("b");
    hls::stream<data_struct> out("out");

    int n_iteration = N_ROWS_A * N_COLS_B;
    int n_points_per_iteration = COMMON_ROWS_COLS;

    a.write((float) n_iteration);
    b.write((float) n_points_per_iteration);

    for (int i = 0; i < N_ROWS_A; i++) {
        for (int j = 0; j < N_COLS_B; j++) {
            for (int k = 0; k < COMMON_ROWS_COLS; k++) {
                a.write(A[i][k]);
                b.write(B[k][j]);
            }
        }
    }

    matr_mul_stream(a, b, out);

    // PRINT RESULT
    for (int i = 0; i < N_ROWS_A; i++) {
        for (int j = 0; j < N_COLS_B; j++) {
            if (j != 0)
                printf("\t|\t");
            printf("%f", out.read().data);
        }
        printf("\n");
    }
    printf("\n");

    return 0;
}
