#include <hls_stream.h>

struct data_struct{
  float data;
  bool last;
};

void matr_mul_stream(hls::stream<float> &x, hls::stream<float> &y, hls::stream<data_struct> &out) {
#pragma HLS INTERFACE axis port=x
#pragma HLS INTERFACE axis port=y
#pragma HLS INTERFACE axis port=out
#pragma HLS INTERFACE ap_ctrl_none port=return

    data_struct out_data;

    int n_iterations = (int) x.read();
    int n_points_per_iteration = (int) y.read();

    float accum = 0;
    accum = accum + x.read() * y.read();

    for (int i = 1; i < n_iterations * n_points_per_iteration; i++) {
#pragma HLS LOOP_TRIPCOUNT avg=108
#pragma HLS PIPELINE II=1

        accum = accum + x.read() * y.read();

        if (i % n_points_per_iteration == n_points_per_iteration - 1) {
            out_data.data = accum;
            out_data.last = (i == (n_iterations * n_points_per_iteration) - 1) ? 1 : 0;

            out.write(out_data);
            accum = 0;
        }
    }

}
