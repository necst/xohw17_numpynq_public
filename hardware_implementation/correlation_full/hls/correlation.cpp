#include <hls_stream.h>

struct data_struct{
  float data;
  bool last;
};

// ============= ARCHITECTURE PARAMETERS =============
// Define the number of points of the autocorrelation function computed each iteration,
// the size of local out buffer and of the shift register.
int const LAGS_PER_ITERATION = 80;
// Define the partition factor of the local buffers.
int const PARTITION_FACTOR = 20;

void correlation( // ============= PARAMETERS =============
        hls::stream<float> &x_in,             // Input stream on which the signal x is passed.
        hls::stream<float> &y_lag_in,         // Input stream on which the signal y is passed, delayed with a certain lag;
                                              // y is passed multiple times, with increasing delay.
        hls::stream<data_struct> &correlation_out     // Output stream, on which the correlation function is written.
        ) {
#pragma HLS INTERFACE axis port=x_in
#pragma HLS INTERFACE axis port=y_lag_in
#pragma HLS INTERFACE axis port=correlation_out
#pragma HLS INTERFACE ap_ctrl_none port=return



    // Data struct to save results
    data_struct out_data;

    // ============= READ PARAMETERS FROM STREAMS =============
    // To avoid add more than two input data access the input parameters are pushed at the beginning of the streams.
    float f_num_points = x_in.read(); // Number of points in the original signal x.
    float f_lag_max = y_lag_in.read(); // Maximum lag of autocorrelation computed.

    int num_points = (int) f_num_points;
    int lag_max = (int) f_lag_max;


    // ============= LOCAL VARIABLES =============
    // The number of iterations uses lag_max rounded up to the closest multiple of LAGS_PER_ITERATIONS;
    // E.g. if lag_max = 95 and LAGS_PER_ITERATIONS, n_iterations = (95 + 5)/10 = 10.
    int n_iterations = (lag_max + LAGS_PER_ITERATION - (lag_max + LAGS_PER_ITERATION) % LAGS_PER_ITERATION) / LAGS_PER_ITERATION;
    float x_i = 0.0;
    float x_i_t = 0.0;

    // Use a local buffer of size LAGS_PER_ITERATION to calculate
    // the autocorrelation points of one iteration.
    float local_correlation_buffer[LAGS_PER_ITERATION];
#pragma HLS ARRAY_PARTITION variable=local_correlation_buffer block factor=PARTITION_FACTOR dim=1

    // Use a shift register to store a certain amount of points of the signal.
    float shift[LAGS_PER_ITERATION];
#pragma HLS ARRAY_PARTITION variable=shift block factor=PARTITION_FACTOR dim=1

    // ============= CORRELATION BUFFER INIT =============
    init_local_correlation_buffer: for (int j = 0; j < LAGS_PER_ITERATION; j++) {
#pragma HLS UNROLL
        local_correlation_buffer[j] = 0.0;
        shift[j] = 0.0;
    }

    // ============= TOT CORRELATION COMPUTATION =============
    iterations: for (int i = 0; i < n_iterations; i++) {
#pragma HLS LOOP_TRIPCOUNT min=2 max=2 avg=2
        // for each iteration

        // ============= SHIFT REGISTER INIT =============
        init_sreg: for (int j = 0; j < LAGS_PER_ITERATION - 1; j++) {
#pragma HLS PIPELINE II=1
            // Initialize the shift register;
            // LAGS_PER_ITERATION - 1 points must be present in the buffer
            // before starting the real computation.
            shift_1: for (int k = LAGS_PER_ITERATION - 1; k > 0; --k) {
#pragma HLS UNROLL
                shift[k] = shift[k - 1];
            }
            shift[0] = y_lag_in.read();

            // Useless reads to keep streams synchronized
            x_in.read();
        }

        // ============= PARTIAL CORRELATION COMPUTATION =============
        one_iteration: for (int n = 0; n < num_points; n++) {
#pragma HLS LOOP_TRIPCOUNT min=105 max=105 avg=105
            // Compute a single iteration, i.e LAGS_PER_ITERATION values of the correlation.
#pragma HLS PIPELINE II=1
            x_i = x_in.read();

            // Shift by 1, and acquire a new point from the signal.
            shift_2: for (int k = LAGS_PER_ITERATION - 1; k > 0; --k) {
#pragma HLS UNROLL
                shift[k] = shift[k - 1];
            }
            shift[0] = y_lag_in.read();

            // ============= PARTIAL SUMS =============
            // Compute the partial sums.
            partial_sums: for (int j = 0; j < LAGS_PER_ITERATION; j++) {
#pragma HLS UNROLL
                local_correlation_buffer[j] += x_i * shift[LAGS_PER_ITERATION - 1 - j];
            }
        }

        // ============= WRITE OUTPUT AND RESET LOCAL CORRELATION BUFFER =============
        // Write LAGS_PER_ITERATION values of the correlation on the output stream.
        write_out: for (int j = 0; j < LAGS_PER_ITERATION; j++) {
#pragma HLS PIPELINE II=1
            out_data.data = local_correlation_buffer[j];
            out_data.last = (j == LAGS_PER_ITERATION - 1) && (i == n_iterations - 1) ? 1 : 0;

            correlation_out.write(out_data);
            local_correlation_buffer[j] = 0.0;
        }
    }
}
