#include <hls_stream.h>
#include "signals.h"
#include <stdio.h>
#include "ap_axi_sdata.h"

struct data_struct {
    float data;
    bool last;
};

#define LAGS_PER_ITERATION 80

void correlation( // ============= PARAMETERS =============
        hls::stream<float> &x_in,             // Input stream on which the signal x is passed.
        hls::stream<float> &y_lag_in,         // Input stream on which the signal y is passed, delayed with a certain lag;
                                              // y is passed multiple times, with increasing delay.
        hls::stream<data_struct> &correlation_out     // Output stream, on which the correlation function is written.
        );

// Simulate the correlation on the cpu, for testing.
// do_covariance = 1 to compute the covariance function instead of the correlation function.
void correlation_cpu(float *x, float *y, float *out, int num_points) {

    for (int i = 0; i < num_points * 2; i++) {
        out[i] = 0.0;
    }

    for (int t = 0; t <= num_points; t++) {
        for (int i = 0; i < num_points - t; i++) {
            out[num_points - 1 - t] += x[i] * y[i + t];
        }
    }

    for (int t = 1; t <= num_points; t++) {
        for (int i = 0; i < num_points - t; i++) {
            out[num_points - 1 + t] += y[i] * x[i + t];
        }
    }
}

void test_correlation(float *x, float *y, int num_points, int lag_max) {


	int PADDED_LAG_MAX = (lag_max + LAGS_PER_ITERATION - (lag_max + LAGS_PER_ITERATION) % LAGS_PER_ITERATION);
	int N_ITERATIONS = (PADDED_LAG_MAX / LAGS_PER_ITERATION);
	int POINTS_PER_ITERATION = (num_points + LAGS_PER_ITERATION - 1);

    hls::stream<float> x_in("x_in");
    hls::stream<float> y_lag_in("y_lag_in");
    hls::stream<data_struct> out("out");


    // Used to store the correlation computed by the (simulated) FPGA;
    float *buff_out = (float *) malloc((lag_max * 2 - 1) * sizeof(float));
    // Used to store the correlation computed by the CPU;
    float *buf_out_cpu = (float *) malloc((lag_max * 2 - 1) * sizeof(float));

    // =========== SW EXECUTION ===========
    correlation_cpu(x, y, buf_out_cpu, num_points);

    // =========== HOW TO CREATE STREAMS ===========
    /*
     * ITERATION 0)
     * stream_1 : [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION - 1) + signal[0, num_points)
     * stream_2 : signal[0, num_points) + [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION - 1)
     *
     * ITERATION i)
     * stream_1 : [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION - 1) + signal[0, num_points)
     * stream_2 : signal[LAGS_PER_ITERATION * i, num_points) + [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION * i) + [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION - 1)
     */

    // Create a padded signal from the original one.
    unsigned long number_of_pads_at_begin = LAGS_PER_ITERATION - 1;
    unsigned long max_number_of_pads_at_end = (LAGS_PER_ITERATION * (N_ITERATIONS)) - 1;
    unsigned long size_of_padded_signal = number_of_pads_at_begin + num_points + max_number_of_pads_at_end;
    float *padded_signal_x = (float*) malloc(size_of_padded_signal * sizeof(float));
    float *padded_signal_y = (float*) malloc(size_of_padded_signal * sizeof(float));

    unsigned long i = 0;
    for (i = 0; i < number_of_pads_at_begin; i++) {
        padded_signal_x[i] = 0.0;
        padded_signal_y[i] = 0.0;
    }
    for (i = 0; i < num_points; i++) {
        padded_signal_x[i + number_of_pads_at_begin] = x[i];
        padded_signal_y[i + number_of_pads_at_begin] = y[i];
    }
    for (i = 0; i < max_number_of_pads_at_end; i++) {
        padded_signal_x[i + number_of_pads_at_begin + num_points] = 0.0;
        padded_signal_y[i + number_of_pads_at_begin + num_points] = 0.0;
    }

    // =========== HW EXECUTION 1 ===========
    // Push the parameters at the beginning of the streams.
    x_in.write((float) num_points);
    y_lag_in.write((float) lag_max);

    // Send signal to the core per iteration.
    for (int i = 0; i < N_ITERATIONS; i++) {
        for (int p = 0; p < POINTS_PER_ITERATION; p++) {
            x_in.write(padded_signal_x[p]);
            y_lag_in.write(padded_signal_y[p + number_of_pads_at_begin + LAGS_PER_ITERATION * i]);
        }
    }

    // Compute the correlation on FPGA
    correlation(x_in, y_lag_in, out);

    // Read results
    for (int f = 0; f < PADDED_LAG_MAX; f++) {
        if (f < lag_max) {
            buff_out[lag_max - 1 - f] = out.read().data;
        } else {
            out.read();
        }
    }

    // =========== HW EXECUTION 2 ===========
    // Push the parameters at the beginning of the streams.
    x_in.write((float) num_points);
    y_lag_in.write((float) lag_max);

    // Send signal to the core per iteration.
    for (int i = 0; i < N_ITERATIONS; i++) {
        for (int p = 0; p < POINTS_PER_ITERATION; p++) {
            x_in.write(padded_signal_y[p]);
            y_lag_in.write(padded_signal_x[p + number_of_pads_at_begin + LAGS_PER_ITERATION * i]);
        }
    }

    // Compute the correlation on FPGA
    correlation(x_in, y_lag_in, out);

    // Read results
    for (int f = 0; f < PADDED_LAG_MAX; f++) {
        if (f < lag_max) {
            buff_out[lag_max - 1 + f] = out.read().data;
        } else {
            out.read();
        }
    }

    // Print results
    printf("\nLAG \t\t | \t GOLDEN     \t | \t CPU     \t | \t FPGA\n");

    for (int i = 0; i < lag_max * 2 - 1; i++) {
        printf("%i \t\t | \t %f \t | \t %f \t | \t %f\n", -(lag_max - 1 - i), golden[i], buf_out_cpu[i], buff_out[i]);
    }

}

int main() {
	float *x = x_105;
    float *y = y_105;
    int num_points = 105;

    test_correlation(x, y, num_points, num_points);
    return 0;
}
