#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "xil_printf.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <xil_cache.h>
#include "xil_types.h"
#include "xil_io.h"
#include "xtmrctr.h"
#include "xparameters.h"
#include "xaxidma.h"
#include "signals.h"
#include "platform.h"

// ------------------------------------------------
// | COSTANT DEPENDING ON ARCHITECTURE            |
// ------------------------------------------------
// Size of internal buffers of the core
#define LAGS_PER_ITERATION 80


// ------------------------------------------------
// | SW FUNCTIONS                                 |
// ------------------------------------------------
// Simulate the correlation on the cpu, for testing.
// do_covariance = 1 to compute the covariance function instead of the correlation function.
void correlation_sw(float *x, float *y, float *out, int num_points) {

	int i, t;

    for (i = 0; i < num_points * 2; i++) {
        out[i] = 0.0;
    }

    for (t = 0; t <= num_points; t++) {
        for (i = 0; i < num_points - t; i++) {
            out[num_points - 1 - t] += x[i] * y[i + t];
        }
    }

    for (t = 1; t <= num_points; t++) {
        for (i = 0; i < num_points - t; i++) {
            out[num_points - 1 + t] += y[i] * x[i + t];
        }
    }
}

/* Function to reverse array from start to end*/
void reverse_array(float *arr, int start, int end)
{
    float temp;
    while (start < end)
    {
        temp = arr[start];
        arr[start] = arr[end];
        arr[end] = temp;
        start++;
        end--;
    }
}

// ------------------------------------------------
// | INITIALIZE DMAs                              |
// ------------------------------------------------
int initDMA(XAxiDma *dma, u32 DeviceId) {
    XAxiDma_Config *CfgPtr;
    int status;

    CfgPtr = XAxiDma_LookupConfig(DeviceId);
    if (!CfgPtr) {
        print("Error looking for AXI DMA config\n\r");
        return XST_FAILURE;
    }
    status = XAxiDma_CfgInitialize(dma, CfgPtr);
    if (status != XST_SUCCESS) {
        print("Error initializing DMA\n\r");
        return XST_FAILURE;
    }
    // Check for scatter gather mode
    if (XAxiDma_HasSg(dma)) {
        print("Error DMA configured in SG mode\n\r");
        return XST_FAILURE;
    }
    // Disable interrupts, we use polling mode
    XAxiDma_IntrDisable(dma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);
    XAxiDma_IntrDisable(dma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DMA_TO_DEVICE);

    // Reset DMA
    XAxiDma_Reset(dma);
    while (!XAxiDma_ResetIsDone(dma)) {
    }
    printf("initialization of DMA %d done!\n", (int) DeviceId);
    return XST_SUCCESS;
}

// ------------------------------------------------
// | HW FUNCTION                                  |
// ------------------------------------------------
int correlation_hw(float *x, float *y, int num_points, int lag_max) {

    init_platform();

    // ========== LOCAL VARIABLES ==========
	int status = 0, i, f, err = 0;

    int padded_lag_max = (lag_max + LAGS_PER_ITERATION - (lag_max + LAGS_PER_ITERATION) % LAGS_PER_ITERATION);
    int n_iterations = (padded_lag_max / LAGS_PER_ITERATION);
    int output_size = padded_lag_max; // The 4 parameters are returned by the core
    int points_per_iteration = num_points + LAGS_PER_ITERATION - 1;

    // To store the results from output stream
    float *buff_out = (float*) malloc(output_size * sizeof(float));
    // Used to store the correlation computed by the SW;
    float *out_sw = (float *) malloc((lag_max * 2 - 1) * sizeof(float));
    // Used to store the correlation computed by the HW;
    float *out_hw = (float *) malloc((lag_max * 2 - 1) * sizeof(float));

    // Parameters are casted to float to be passed to the streams.
    float f_num_points = (float) num_points;
    float f_lag_max = (float) lag_max;


    // ========== INITIALIZE ENVIRONMENT ==========
    // Disable the cache
    Xil_DCacheDisable();
    Xil_ICacheDisable();

	// Declaration and Initialization of the HW timer
	unsigned int begin_time;
	unsigned int end_time;
	unsigned int calibration;
	double run_time_sw = 0;
	double run_time_hw = 0;

	XTmrCtr timer;

	status = XTmrCtr_Initialize(&timer, XPAR_AXI_TIMER_0_DEVICE_ID);
	if (status != XST_SUCCESS) {
		print("Error: timer setup failed\n");
		return XST_FAILURE;
	}
	XTmrCtr_SetOptions(&timer, 0,
	XTC_AUTO_RELOAD_OPTION | XTC_CASCADE_MODE_OPTION);

	XTmrCtr_Reset(&timer, 0);
	XTmrCtr_Reset(&timer, 1);

	// Calibrate HW timer
	XTmrCtr_Start(&timer, 0);
	begin_time = XTmrCtr_GetValue(&timer, 0);
	end_time = XTmrCtr_GetValue(&timer, 0);
	XTmrCtr_Stop(&timer, 0);

	calibration = end_time - begin_time;

    // Declaration and Initialization of the DMAs
    XAxiDma dma_0;
    XAxiDma dma_1;

	status = initDMA(&dma_0, 0);
	if (status != XST_SUCCESS) {
		print("\rError: DMA init failed\n");
		return XST_FAILURE;
	}

	status = initDMA(&dma_1, 1);
	if (status != XST_SUCCESS) {
		print("\rError: DMA init failed\n");
		return XST_FAILURE;
	}


	// ========== SOFTWARE EXECUTION ==========
	printf("\n====================\nSW EXECUTION:\n====================\n");

	// Start HW timer
	XTmrCtr_Start(&timer, 0);
	begin_time = XTmrCtr_GetValue(&timer, 0);

	// Execution on CPU
	correlation_sw(x, y, out_sw, num_points);

	// Stop timer
	end_time = XTmrCtr_GetValue(&timer, 0);
	XTmrCtr_Stop(&timer, 0);

	// Compute time
	run_time_sw = (end_time - begin_time - calibration) / 100000000.0;
	printf("CPU Execution Time: %f \n", run_time_sw);


	// ========== HARDWARE EXECUTION ==========
	printf("\n====================\nHW EXECUTION:\n====================\n");
	// Start timer
	XTmrCtr_Start(&timer, 0);
	begin_time = XTmrCtr_GetValue(&timer, 0);


    // =========== HOW TO CREATE STREAMS ===========
    /*
     * ITERATION 0)
     * stream_1 : [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION - 1) + signal[0, num_points)
     * stream_2 : signal[0, num_points) + [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION - 1)
     *
     * ITERATION i)
     * stream_1 : [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION - 1) + signal[0, num_points)
     * stream_2 : signal[LAGS_PER_ITERATION * i, num_points) + [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION * i) + [0.0, 0.0, ..., 0.0] x (LAGS_PER_ITERATION - 1)
     */

    // Create a padded signal from the original one (calloc set allocated buffer to 0).
    int number_of_pads_at_begin = LAGS_PER_ITERATION - 1;
    int max_number_of_pads_at_end = (LAGS_PER_ITERATION * (n_iterations)) - 1;
    int size_of_padded_signal = number_of_pads_at_begin + num_points + max_number_of_pads_at_end;
    float *padded_signal_x = (float*) calloc(size_of_padded_signal, sizeof(float));
    float *padded_signal_y = (float*) calloc(size_of_padded_signal, sizeof(float));

    memcpy(&padded_signal_x[number_of_pads_at_begin], x, num_points * sizeof(float));
    memcpy(&padded_signal_y[number_of_pads_at_begin], y, num_points * sizeof(float));


	// @@@@@@@@@@ FIRST STEP @@@@@@@@@@@@@@@
    // ========== SEND PARAMETERS ==========
    // Push each parameter at the beginning of the streams and wait until they are received by the core.
    XAxiDma_SimpleTransfer(&dma_0, (u32) &f_num_points, sizeof(float),
    XAXIDMA_DMA_TO_DEVICE);
    while (XAxiDma_Busy(&dma_0, XAXIDMA_DMA_TO_DEVICE))
        ;
    XAxiDma_SimpleTransfer(&dma_1, (u32) &f_lag_max, sizeof(float),
    XAXIDMA_DMA_TO_DEVICE);
    while (XAxiDma_Busy(&dma_1, XAXIDMA_DMA_TO_DEVICE))
        ;
    // Now all parameters are sent by processor and received by the core.

    // ========== OPEN STREAM TO READ RESULTS ==========
    // Open stream to receive data from the core and start read as soon as the data are available
    XAxiDma_SimpleTransfer(&dma_0, (u32) &buff_out[0], padded_lag_max * sizeof(float),
    XAXIDMA_DEVICE_TO_DMA);

    // ========== SEND SIGNAL ==========
    // Send signal to the core per iteration.
    for (i = 0; i < n_iterations; i++) {

            XAxiDma_SimpleTransfer(&dma_0,
					(u32) &padded_signal_x[0],
					points_per_iteration * sizeof(float),
					XAXIDMA_DMA_TO_DEVICE);

			XAxiDma_SimpleTransfer(&dma_1,
					(u32) &padded_signal_y[number_of_pads_at_begin + LAGS_PER_ITERATION * i],
							points_per_iteration * sizeof(float),
					XAXIDMA_DMA_TO_DEVICE);

			// Wait until data of this transfer are received.
			while (XAxiDma_Busy(&dma_0, XAXIDMA_DMA_TO_DEVICE)
					|| XAxiDma_Busy(&dma_1, XAXIDMA_DMA_TO_DEVICE))
				;
    }

	// ========== WAIT RESULTS ==========
	// Wait until all data of output are read from the core.
	while (XAxiDma_Busy(&dma_0, XAXIDMA_DEVICE_TO_DMA))
		;

    // REVERSE THE RESULTS
    reverse_array(buff_out, 0, lag_max - 1);

	// Copy valid results to output buff
    memcpy(&out_hw[0], buff_out, lag_max * sizeof(float));


	// @@@@@@@@@@ SECOND STEP @@@@@@@@@@@@@@@
    // ========== SEND PARAMETERS ==========
    // Push each parameter at the beginning of the streams and wait until they are received by the core.
    XAxiDma_SimpleTransfer(&dma_0, (u32) &f_num_points, sizeof(float),
    XAXIDMA_DMA_TO_DEVICE);
    while (XAxiDma_Busy(&dma_0, XAXIDMA_DMA_TO_DEVICE))
        ;
    XAxiDma_SimpleTransfer(&dma_1, (u32) &f_lag_max, sizeof(float),
    XAXIDMA_DMA_TO_DEVICE);
    while (XAxiDma_Busy(&dma_1, XAXIDMA_DMA_TO_DEVICE))
        ;
    // Now all parameters are sent by processor and received by the core.

    // ========== OPEN STREAM TO READ RESULTS ==========
    // Open stream to receive data from the core and start read as soon as the data are available
    XAxiDma_SimpleTransfer(&dma_0, (u32) &buff_out[0], padded_lag_max * sizeof(float),
    XAXIDMA_DEVICE_TO_DMA);

    // ========== SEND SIGNAL ==========
    // Send signal to the core per iteration.
    for (i = 0; i < n_iterations; i++) {

            XAxiDma_SimpleTransfer(&dma_0,
					(u32) &padded_signal_y[0],
					points_per_iteration * sizeof(float),
					XAXIDMA_DMA_TO_DEVICE);

			XAxiDma_SimpleTransfer(&dma_1,
					(u32) &padded_signal_x[number_of_pads_at_begin + LAGS_PER_ITERATION * i],
							points_per_iteration * sizeof(float),
					XAXIDMA_DMA_TO_DEVICE);

			// Wait until data of this transfer are received.
			while (XAxiDma_Busy(&dma_0, XAXIDMA_DMA_TO_DEVICE)
					|| XAxiDma_Busy(&dma_1, XAXIDMA_DMA_TO_DEVICE))
				;
    }

	// ========== WAIT RESULTS ==========
	// Wait until all data of output are read from the core.
	while (XAxiDma_Busy(&dma_0, XAXIDMA_DEVICE_TO_DMA))
		;

	// Copy valid results to output buff
    memcpy(&out_hw[lag_max - 1], buff_out, lag_max * sizeof(float));



	// Stop timer
	end_time = XTmrCtr_GetValue(&timer, 0);

	XTmrCtr_Stop(&timer, 0);

	run_time_hw = (end_time - begin_time - calibration) / 100000000.0;
	printf("FPGA Execution Time: %f \n", run_time_hw);


    // Print results
    int PRINT_RESULTS = 0;

    if (PRINT_RESULTS) {
        printf("\nLAG \t\t | \t GOLDEN     \t | \t CPU     \t | \t FPGA\n");
        for (i = 0; i < lag_max * 2 - 1; i++) {
            printf("%i \t\t | \t %f \t | \t %f \t | \t %f\n", -(lag_max - 1 - i), golden[i], out_sw[i], out_hw[i]);
        }
    }

	// Results comparison
	err = 0;
	for (i = 0; (i < lag_max * 2 - 1 && !err); i++)
		if (out_sw[i] != out_hw[i])
			err++;

	// RESULT
	if (err == 0){
		printf("Results identical... Test successful!\n");

		// SPEEDUP
		printf("SPEEDUP: %f \n", run_time_sw / run_time_hw);
	} else{
		printf("Test failed!\n");
	}

    printf("\nEND\n");

    cleanup_platform();
    return err;
}

int main() {
	float *x = x_105;
    float *y = y_105;
    int num_points = 105;

    correlation_hw(x, y, num_points, num_points);
    return 0;
}
