#### Vivado HLS 2015.4 ####
- New Project (PYNQ Part: xc7z020clg400-1)

- Suorce: correlation.cpp
- Test: correlation_test.cpp signals.h

- Simulation
- Synthesis (set top function (Correlation); set target time 10 (10ns == 100Mhz)
- Cosimulation
- Solution -> Export RTL


#### Vivado 2015.4 ####
- New Project (PYNQ Part: xc7z020clg400-1)

- Create Block Design

- Import Cores:
-- IP settting
-- Repository Management
-- Add Cores by selecting HLS directory

===========================

-- download: Zynq Presets https://reference.digilentinc.com/reference/programmable-logic/pynq-z1/start
--- add: ZYNQ7 Processing System
        . double click -> preset -> apply configuration -> select .tcl
        . PS-PL Configuration -> Axi non secure -> GP Master -> Enable GP0 (to communicate through axi lite)
        . PS-PL Configuration -> HP Slave Axi -> Enable HP0
        . Autoconnect
--- add: AXI DMA
        . double click -> 
            . Disable Scatter Gather Engine 
            . Buffer Length 23 
            . Select data width (eg. 32 == sizeof(float)) 
            . Mux Burst Size 256 
            . Allow Unaligned Transfer
        . Autoconnect
--- copy-paste: AXI DMA
        . double click -> 
            . Disable Write Channel
        . Autoconnect
--- add: core (mmult_84)
        . connect stream Interfaces to DMA Read and Write Channels
        . connect clk to DMA clk
        . connect reset to DMA reset
--- add: AXI Timer
        . double click -> Enable 64 bit mode
        . Autoconnect

===========================

- Validate Design
- Sources -> Design (dx click) -> create HDL Wrapper [needed to create drivers interfaces]
- Generate Bitstream
- File -> Export -> Hardware -> Include Bitstream
- File -> Launch SDK


#### SDK 2016.1 ####
- File -> New Application Proj -> (check correct .hdf (if it is to import select Target HW -> New -> Browse..)) -> Next (Helloworld App)

- App -> Src -> Iscript -> (augment Heap and Stack (eg. 0xffffff))

- Connect Board

- Program FPGA -> Program

- Run Configurations..  -> Xilinx Application (GDB)
--- Target Setup (Bitstream file: [select] / Initialization file: [select] / Reset Processor: select / Program FPGA: activate / Run ps7_init: activate / Run ps7_post_config: activate)
--- Application (select application and Proj name)
--- STDIO Connection (Connect STDIO to console -> Serial port (eg. COM5 115200))
--------- if console doesn't work on Linux environment: sudo screen /dev/ttyUSB2 115200 (to print into terminal the output of selected port eg. ttyUSB2)

- Run
