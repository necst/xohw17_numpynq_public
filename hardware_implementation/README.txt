Each hardware implementation folder contains three subfolders to build and test functions by using Vivado HLS, Vivado and Vivado SDK.
To build and test functions, please refer to the README file contained in each hardware implementation folder.
All source files are contained in the subfolder of the specific tool that uses them.