#include <hls_stream.h>

struct data_struct{
  int data;
  bool last;
};

/* PSEUDOCODE TO UNDERSTAND THE BENEATH FLATTENED LOOP

    n_iterations = x.pop();
    n_points_per_iteration = y.pop();

    for (i = 0; i < n_iterations; i++) {
        accum_row_col = 0;
        
        for (j = 0; j < n_points_per_iteration; j++)
            accum_row_col = accum_row_col + x.pop() * y.pop();
        
        out.push(accum_row_col);
    }
    
*/

void matr_mul_stream(hls::stream<int> &x, hls::stream<int> &y, hls::stream<data_struct> &out) {
#pragma HLS INTERFACE axis port=x
#pragma HLS INTERFACE axis port=y
#pragma HLS INTERFACE axis port=out
#pragma HLS INTERFACE ap_ctrl_none port=return
//#pragma HLS INTERFACE s_axilite port=return

    data_struct out_data;

    int n_iterations = x.read();
    int n_points_per_iteration = y.read();

    int accum = 0;
    accum = accum + x.read() * y.read();

    for (int i = 1; i < n_iterations * n_points_per_iteration; i++) {
#pragma HLS LOOP_TRIPCOUNT avg=108
#pragma HLS PIPELINE II=1

        accum = accum + x.read() * y.read();

        if (i % n_points_per_iteration == n_points_per_iteration - 1) {
            out_data.data = accum;
            out_data.last = (i == (n_iterations * n_points_per_iteration) - 1) ? 1 : 0;

            out.write(out_data);
            accum = 0;
        }
    }

}
