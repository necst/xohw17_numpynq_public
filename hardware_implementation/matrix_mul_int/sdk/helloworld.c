#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "xil_printf.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <xil_cache.h>
#include "xil_types.h"
#include "xil_io.h"
#include "xtmrctr.h"
#include "xparameters.h"
#include "xaxidma.h"
#include "platform.h"
//#include "xmatr_mul_stream.h" // Not necessary if "HLS INTERFACE ap_ctrl_none"


// ------------------------------------------------
// |      COSTANT DEPENDING ON ARCHITECTURE       |
// ------------------------------------------------

// None


// ------------------------------------------------
// |     SOFTWARE EXECUTION TO COMPARE RESULTS    |
// ------------------------------------------------
#define N_ROWS_A 6
#define COMMON_ROWS_COLS 9
#define N_COLS_B 2

void matrix_mul_sw(int A[N_ROWS_A][COMMON_ROWS_COLS],
		int B[COMMON_ROWS_COLS][N_COLS_B], int C[N_ROWS_A][N_COLS_B]) {
	int i, j, k;
	for (i = 0; i < N_ROWS_A; i++) {
		for (j = 0; j < N_COLS_B; j++) {
			for (k = 0; k < COMMON_ROWS_COLS; k++) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}
}


// ------------------------------------------------
// |                INITIALIZE DMAs               |
// ------------------------------------------------
int initDMA(XAxiDma *dma, u32 DeviceId) {
	XAxiDma_Config *CfgPtr;
	int status;

	CfgPtr = XAxiDma_LookupConfig(DeviceId);
	if (!CfgPtr) {
		print("Error looking for AXI DMA config\n\r");
		return XST_FAILURE;
	}
	status = XAxiDma_CfgInitialize(dma, CfgPtr);
	if (status != XST_SUCCESS) {
		print("Error initializing DMA\n\r");
		return XST_FAILURE;
	}
	// Check for scatter gather mode
	if (XAxiDma_HasSg(dma)) {
		print("Error DMA configured in SG mode\n\r");
		return XST_FAILURE;
	}
	// Disable interrupts, we use polling mode
	XAxiDma_IntrDisable(dma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(dma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DMA_TO_DEVICE);

	// Reset DMA
	XAxiDma_Reset(dma);
	while (!XAxiDma_ResetIsDone(dma)) {
	}

	printf("initialization of DMA %d done!\n", (int) DeviceId);
	return XST_SUCCESS;
}


// ------------------------------------------------
// |          FUNCTION THAT TEST CORE             |
// ------------------------------------------------
int main() {

	init_platform();

	// ========== LOCAL VARIABLES ==========
	int status, err=0, i, j, k, h;

    int A[N_ROWS_A][COMMON_ROWS_COLS] = {
        {1,2,3,0,2,4,5,6,3},
        {4,5,6,1,2,9,8,6,4},
        {0,2,3,2,3,3,2,0,8},
        {9,8,1,4,5,6,1,2,9},
        {4,5,6,1,2,9,8,6,4},
        {9,8,1,4,5,6,1,2,9}
    };

    int B[COMMON_ROWS_COLS][N_COLS_B] = {
        {4,1},
        {0,5},
        {3,2},
        {3,2},
        {3,2},
        {3,2},
        {0,5},
        {0,5},
        {0,5}
    };

	int OUT_SW[N_ROWS_A][N_COLS_B];
	int OUT_HW[N_ROWS_A][N_COLS_B];


	// ========== PARAMETERS NEEDED BY THE CORE ==========
	int n_iterations = N_ROWS_A * N_COLS_B;
	int n_points_per_iteration = COMMON_ROWS_COLS;
	int tot_points_streams_in = n_points_per_iteration * n_iterations;
	int tot_points_streams_out = n_iterations;

	// Buffers to send (receive) data to (from) streams
	int *a = (int*) malloc(tot_points_streams_in * sizeof(int));
	int *b = (int*) malloc(tot_points_streams_in * sizeof(int));
	int *buff_out = (int*) malloc(tot_points_streams_out * sizeof(int));


	// ========== INIT OUT MATRIXES ==========
	for (i = 0; i < N_ROWS_A; i++) {
		for (j = 0; j < N_COLS_B; j++) {
			OUT_SW[i][j] = 0;
			OUT_HW[i][j] = 0;
		}
	}


	// ========== FILL BUFFERS TO STREAM DATA WITH THE CORE ==========
	h = 0;
	for (i = 0; i < N_ROWS_A; i++) {
		for (j = 0; j < N_COLS_B; j++) {
			for (k = 0; k < COMMON_ROWS_COLS; k++) {
				a[h] = A[i][k];
				b[h] = B[k][j];
				h++;
			}
		}
	}

	for (i = 0; i < N_ROWS_A; i++) {
		for (j = 0; j < N_COLS_B; j++) {
			buff_out[i * N_COLS_B + j] = 0;
		}
	}


	// ========== INITIALIZE ENVIRONMENT ==========
	// Declaration of the core
	//XMatr_mul_stream core; // Not necessary if "HLS INTERFACE ap_ctrl_none"

	// Initialization of the core
	//XMatr_mul_stream_Initialize(&core, 0); // Not necessary if "HLS INTERFACE ap_ctrl_none"
	//XMatr_mul_stream_DisableAutoRestart(&core); // Not necessary if "HLS INTERFACE ap_ctrl_none"

	// Disable the cache
	Xil_DCacheDisable();
	Xil_ICacheDisable();

	// Declaration and Initialization of the HW timer
	unsigned int begin_time;
	unsigned int end_time;
	unsigned int calibration;
	double run_time_sw = 0;
	double run_time_hw = 0;

	XTmrCtr timer;

	status = XTmrCtr_Initialize(&timer, XPAR_AXI_TIMER_0_DEVICE_ID);
	if (status != XST_SUCCESS) {
		print("Error: timer setup failed\n");
		return XST_FAILURE;
	}
	XTmrCtr_SetOptions(&timer, 0,
			XTC_AUTO_RELOAD_OPTION | XTC_CASCADE_MODE_OPTION);

	XTmrCtr_Reset(&timer, 0);
	XTmrCtr_Reset(&timer, 1);

	// Calibrate HW timer
	XTmrCtr_Start(&timer, 0);
	begin_time = XTmrCtr_GetValue(&timer, 0);
	end_time = XTmrCtr_GetValue(&timer, 0);
	XTmrCtr_Stop(&timer, 0);

	calibration = end_time - begin_time;

	// Declaration and Initialization of the DMAs
	XAxiDma dma_0;
	XAxiDma dma_1;

	status = initDMA(&dma_0, 0);
	if (status != XST_SUCCESS) {
		print("\rError: DMA init failed\n");
		return XST_FAILURE;
	}

	status = initDMA(&dma_1, 1);
	if (status != XST_SUCCESS) {
		print("\rError: DMA init failed\n");
		return XST_FAILURE;
	}


	// ========== SOFTWARE EXECUTION ==========
	printf("\n====================\nSW MATRIX MUL:\n====================\n");

	// Start HW timer
	XTmrCtr_Start(&timer, 0);
	begin_time = XTmrCtr_GetValue(&timer, 0);

	// Execution on CPU
	matrix_mul_sw(A, B, OUT_SW);

	// Stop timer
	end_time = XTmrCtr_GetValue(&timer, 0);
	XTmrCtr_Stop(&timer, 0);

	// Compute time
	run_time_sw = (end_time - begin_time - calibration) / 100000000.0;
	printf("CPU Execution Time: %f \n", run_time_sw);

	// PRINT SW RESULT
	for (i = 0; i < N_ROWS_A; i++) {
		for (j = 0; j < N_COLS_B; j++) {
			if (j != 0)
				printf("\t|\t");
			printf("%d", OUT_SW[i][j]);
		}
		printf("\n");
	}
	printf("\n");


	// ========== HARDWARE EXECUTION ==========
	printf("\n====================\nHW MATRIX MUL:\n====================\n");
	// Start timer
	XTmrCtr_Start(&timer, 0);
	begin_time = XTmrCtr_GetValue(&timer, 0);

	// ========== START CORE ==========
	//XMatr_mul_stream_Start(&core); // Not necessary if "HLS INTERFACE ap_ctrl_none"

	// ========== OPEN STREAM TO READ RESULTS ==========
	// Open stream to receive data from the core and start read as soon as the data are available
	XAxiDma_SimpleTransfer(&dma_0, (u32) buff_out,
			tot_points_streams_out * sizeof(int), XAXIDMA_DEVICE_TO_DMA);

	// ========== SEND PARAMETERS ==========
	// Push each parameter at the beginning of the streams
	XAxiDma_SimpleTransfer(&dma_0, (u32) &n_iterations, sizeof(int),
			XAXIDMA_DMA_TO_DEVICE);
	while (XAxiDma_Busy(&dma_0, XAXIDMA_DMA_TO_DEVICE))
		;
	XAxiDma_SimpleTransfer(&dma_1, (u32) &n_points_per_iteration, sizeof(int),
			XAXIDMA_DMA_TO_DEVICE);
	while (XAxiDma_Busy(&dma_1, XAXIDMA_DMA_TO_DEVICE))
		;
	// Now all parameters are sent by processor and received by the core.

	// ========== SEND SIGNALS ==========
	// Send signals to the core per iteration.
	for (i = 0; i < n_iterations; i++) {

		XAxiDma_SimpleTransfer(&dma_0, (u32) &a[i * n_points_per_iteration],
				n_points_per_iteration * sizeof(int),
				XAXIDMA_DMA_TO_DEVICE);

		XAxiDma_SimpleTransfer(&dma_1, (u32) &b[i * n_points_per_iteration],
				n_points_per_iteration * sizeof(int),
				XAXIDMA_DMA_TO_DEVICE);

		// Wait until data of this transfer are received.
		while (XAxiDma_Busy(&dma_0, XAXIDMA_DMA_TO_DEVICE)
				|| XAxiDma_Busy(&dma_1, XAXIDMA_DMA_TO_DEVICE))
			;

	}
	// Ensure all data are received.
	while (XAxiDma_Busy(&dma_0, XAXIDMA_DMA_TO_DEVICE)
			|| XAxiDma_Busy(&dma_1, XAXIDMA_DMA_TO_DEVICE))
		;

	// ========== WAIT RESULTS ==========
	// Wait until all data of output are read from the core.
	while (XAxiDma_Busy(&dma_0, XAXIDMA_DEVICE_TO_DMA))
		;

	// Stop timer
	end_time = XTmrCtr_GetValue(&timer, 0);

	XTmrCtr_Stop(&timer, 0);

	run_time_hw = (end_time - begin_time - calibration) / 100000000.0;
	printf("FPGA Execution Time: %f \n", run_time_hw);

	// Write results
	for (i = 0; i < N_ROWS_A; i++) {
		for (j = 0; j < N_COLS_B; j++) {
			OUT_HW[i][j] = buff_out[i * N_COLS_B + j];
		}
	}

	// PRINT HW RESULT
	for (i = 0; i < N_ROWS_A; i++) {
		for (j = 0; j < N_COLS_B; j++) {
			if (j != 0)
				printf("\t|\t");
			printf("%d", OUT_HW[i][j]);
		}
		printf("\n");
	}
	printf("\n\n");

	// COMPARE THE RESULTS FROM SW AND HW
	for (i = 0; i < N_ROWS_A; i++)
		for (j = 0; j < N_COLS_B; j++)
			if (OUT_SW[i][j] != OUT_HW[i][j]) {
				err = 1;
			}

	if (err == 0)
		print("SW and HW results match!\n\n");
	else
		print("ERROR: results mismatch\n\n");

	// HW vs. SW speedup factor
	double acc_factor = run_time_sw / run_time_hw;
	xil_printf("Speedup: %d.%d \n\n", (int) acc_factor,
			(int) (acc_factor * 1000) % 1000);

	cleanup_platform();
	return XST_SUCCESS;
}
