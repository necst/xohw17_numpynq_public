#include <stdio.h>
#include "xtime_l.h"
#include "platform.h"
#include "xil_printf.h"
#include "xsum_hw.h"

int main() {
	init_platform();

	// ========== LOCAL VARIABLES ==========
	int a = 3;
	int b = 5;
	int c = 0;
	int d = 0;
	XTime tStart = 0, tEnd = 0, tExecCPU = 0, tExecFPGA = 0;
	float speedup = 0;

	// @@@@@@@@@@@@@@ CPU EXECUTION @@@@@@@@@@@@@@
	XTime_GetTime(&tStart);
	c = a + b;
	XTime_GetTime(&tEnd);
	tExecCPU = tEnd - tStart;

	// ========== INITIALIZE ENVIRONMENT ==========
	// Declaration of the core
	XSum_hw core0;

	// Initialization of the core
	XSum_hw_Initialize(&core0, 0);
	XSum_hw_DisableAutoRestart(&core0);

	// Disable the cache
	Xil_DCacheDisable();
	Xil_ICacheDisable();

	// @@@@@@@@@@@@@@ FPGA EXECUTION @@@@@@@@@@@@@@
	XTime_GetTime(&tStart);

	// Set the axilite parameters
	XSum_hw_Set_a(&core0, a);
	XSum_hw_Set_b(&core0, b);

	// ========== START CORE ==========
	XSum_hw_Start(&core0);

	// wait until core is done
	while (!XSum_hw_IsDone(&core0))
		;

	// read the result
	d = XSum_hw_Get_return(&core0);

	XTime_GetTime(&tEnd);
	tExecFPGA = tEnd - tStart;

	speedup = (float) tExecCPU/tExecFPGA;

	// ========== RESULTS ==========
	printf("CPU result: %d\n", c);
	printf("FPGA result: %d\n", d);
	printf("CPU clock cycles: %lld\n", tExecCPU);
	printf("FPGA clock cycles: %lld\n", tExecFPGA);
	printf("Speedup: %f\n", speedup);

	cleanup_platform();
	return 0;
}
