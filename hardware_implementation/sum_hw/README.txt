#### Vivado HLS 2016.3 ####
- New Project (PYNQ Part: xc7z020clg400-1)

- Suorce/Test [...]

- Simulation
- Synthesis (set target time (eg. 10ns == 100Mhz)
- Cosimulation
- Solution -> Export RTL


#### Vivado 2016.3 ####
- New Project (PYNQ Part: xc7z020clg400-1)

- Create Block Design

- Import Cores:
-- IP settting
-- Repository Management
-- Add Cores by selecting HLS directory

===========================

[Board PYNQ]
-- download: PYNQ-Z1 Zynq Presets https://reference.digilentinc.com/reference/programmable-logic/pynq-z1/start
--- add: ZYNQ7 Processing System (double click -> add preset ...) (PS-PL Configuration -> Axi non secure -> GP Master -> Enable GP0 (to communicate through axi lite))
--- add: core and autoconnect

===========================

- Validate Design

- Sources -> Design (dx click) -> create HDL Wrapper [needed to create drivers interfaces]

- Generate Bitstream

- File -> Export -> Hardware -> Include Bitstream

- File -> Launch SDK


#### SDK 2016.3 ####
- File -> New Application Proj -> (check correct .hdf (if it is to import select Target HW -> New -> Browse.. and Pray)) -> [...] (Helloworld)

- Src -> Iscript -> (augment Heap and Stack: 0x1f000000 or less if error)

- Connect PYNQ

- Program FPGA -> Program

- Run Configurations..  -> Xilinx Application (GDB)
--- Target Setup (Bitstream file: [select] / Initialization file: [select] / Reset Processor: select / Program FPGA: activate / Run ps7_init: activate / Run ps7_post_config: activate)
--- Application (select application and Proj name)
--- STDIO Connection (Connect STDIO to console -> Try port (eg. COM5 115200))
--------- if console doesn't work: sudo screen /dev/ttyUSB2 115200 (to print into terminal the output of selected port eg. ttyUSB2)

- Run
