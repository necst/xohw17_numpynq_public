#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "xil_printf.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <xil_cache.h>
#include "xil_types.h"
#include "xil_io.h"
#include "xtmrctr.h"
#include "xparameters.h"
#include "xaxidma.h"
#include "platform.h"

#define DIM 84
#define PRINT_RES 0

// ------------------------------------------------
// |                INITIALIZE DMAs               |
// ------------------------------------------------
int initDMA(XAxiDma *dma, u32 DeviceId) {
	XAxiDma_Config *CfgPtr;
	int status;

	CfgPtr = XAxiDma_LookupConfig(DeviceId);
	if (!CfgPtr) {
		print("Error looking for AXI DMA config\n\r");
		return XST_FAILURE;
	}
	status = XAxiDma_CfgInitialize(dma, CfgPtr);
	if (status != XST_SUCCESS) {
		print("Error initializing DMA\n\r");
		return XST_FAILURE;
	}
	// Check for scatter gather mode
	if (XAxiDma_HasSg(dma)) {
		print("Error DMA configured in SG mode\n\r");
		return XST_FAILURE;
	}
	// Disable interrupts, we use polling mode
	XAxiDma_IntrDisable(dma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(dma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DMA_TO_DEVICE);

	// Reset DMA
	XAxiDma_Reset(dma);
	while (!XAxiDma_ResetIsDone(dma)) {
	}

	printf("initialization of DMA %d done!\n", (int) DeviceId);
	return XST_SUCCESS;
}

// ------------------------------------------------
// |                SW function                   |
// ------------------------------------------------
void mmult_sw(float a[DIM][DIM], float b[DIM][DIM], float out[DIM][DIM]) {
	// matrix multiplication of a A*B matrix
	int ia, ib, id;
	for (ia = 0; ia < DIM; ++ia)
		for (ib = 0; ib < DIM; ++ib) {

			float sum = 0;

			for (id = 0; id < DIM; ++id)

				sum += a[ia][id] * b[id][ib];

			out[ia][ib] = sum;
		}
}

// ------------------------------------------------
// |          FUNCTION THAT TEST CORE             |
// ------------------------------------------------
int main() {
	init_platform();

	int status, i, j, err = 0;

	float matOp1[DIM][DIM];
	float matOp2[DIM][DIM];
	float matMult_sw[DIM][DIM];
	float matMult_hw[DIM][DIM];

	// Matrix Initiation
	for (i = 0; i < DIM; i++) {
		for (j = 0; j < DIM; j++) {
			matOp1[i][j] = (float) (i+j);
			matOp2[i][j] = (float) (i*j);
		}
	}

	// ========== INITIALIZE ENVIRONMENT ==========
	// Disable the cache
	Xil_DCacheDisable();
	Xil_ICacheDisable();

	// Declaration and Initialization of the HW timer
	unsigned int begin_time;
	unsigned int end_time;
	unsigned int calibration;
	double run_time_sw = 0;
	double run_time_hw = 0;

	XTmrCtr timer;

	status = XTmrCtr_Initialize(&timer, XPAR_AXI_TIMER_0_DEVICE_ID);
	if (status != XST_SUCCESS) {
		print("Error: timer setup failed\n");
		return XST_FAILURE;
	}
	XTmrCtr_SetOptions(&timer, 0,
	XTC_AUTO_RELOAD_OPTION | XTC_CASCADE_MODE_OPTION);

	XTmrCtr_Reset(&timer, 0);
	XTmrCtr_Reset(&timer, 1);

	// Calibrate HW timer
	XTmrCtr_Start(&timer, 0);
	begin_time = XTmrCtr_GetValue(&timer, 0);
	end_time = XTmrCtr_GetValue(&timer, 0);
	XTmrCtr_Stop(&timer, 0);

	calibration = end_time - begin_time;

	// Declaration and Initialization of the DMAs
	XAxiDma dma_0;

	status = initDMA(&dma_0, 0);
	if (status != XST_SUCCESS) {
		print("\rError: DMA init failed\n");
		return XST_FAILURE;
	}

	// ========== SOFTWARE EXECUTION ==========
	printf("\n====================\nSW MATRIX MUL:\n====================\n");

	// Start HW timer
	XTmrCtr_Start(&timer, 0);
	begin_time = XTmrCtr_GetValue(&timer, 0);

	// Execution on CPU
	mmult_sw(matOp1, matOp2, matMult_sw);

	// Stop timer
	end_time = XTmrCtr_GetValue(&timer, 0);
	XTmrCtr_Stop(&timer, 0);

	// Compute time
	run_time_sw = (end_time - begin_time - calibration) / 100000000.0;
	printf("CPU Execution Time: %f \n", run_time_sw);

	// PRINT SW RESULT
	if (PRINT_RES) {
		for (i = 0; i < DIM; i++) {
			for (j = 0; j < DIM; j++) {
				if (j != 0)
					printf("\t|\t");
				printf("%f", matMult_sw[i][j]);
			}
			printf("\n");
		}
		printf("\n");
	}

	// ========== HARDWARE EXECUTION ==========
	printf("\n====================\nHW MATRIX MUL:\n====================\n");
	// Start timer
	XTmrCtr_Start(&timer, 0);
	begin_time = XTmrCtr_GetValue(&timer, 0);

	// ========== OPEN STREAM TO READ RESULTS ==========
	// Open stream to receive data from the core and start read as soon as the data are available
	XAxiDma_SimpleTransfer(&dma_0, (u32) &(matMult_hw[0][0]), (DIM * DIM) * sizeof(float),
			XAXIDMA_DEVICE_TO_DMA);

	// ========== SEND MATRIXES ==========
	// Send points to the core
	XAxiDma_SimpleTransfer(&dma_0, (u32) &(matOp1[0][0]), DIM * DIM * sizeof(float),
			XAXIDMA_DMA_TO_DEVICE);
	// Wait until data of this transfer are received.
	while (XAxiDma_Busy(&dma_0, XAXIDMA_DMA_TO_DEVICE))
		;

	XAxiDma_SimpleTransfer(&dma_0, (u32) &(matOp2[0][0]), DIM * DIM * sizeof(float),
			XAXIDMA_DMA_TO_DEVICE);
	// Wait until data of this transfer are received.
	while (XAxiDma_Busy(&dma_0, XAXIDMA_DMA_TO_DEVICE))
		;

	// ========== WAIT RESULTS ==========
	// Wait until all data of output are read from the core.
	while (XAxiDma_Busy(&dma_0, XAXIDMA_DEVICE_TO_DMA))
		;

	// Stop timer
	end_time = XTmrCtr_GetValue(&timer, 0);

	XTmrCtr_Stop(&timer, 0);

	run_time_hw = (end_time - begin_time - calibration) / 100000000.0;
	printf("FPGA Execution Time: %f \n", run_time_hw);


	// PRINT HW RESULT
	if (PRINT_RES) {
		for (i = 0; i < DIM; i++) {
			for (j = 0; j < DIM; j++) {
				if (j != 0)
					printf("\t|\t");
				printf("%f", matMult_hw[i][j]);
			}
			printf("\n");
		}
		printf("\n");
	}

	// Matrix comparison
	err = 0;
	for (i = 0; (i < DIM && !err); i++)
		for (j = 0; (j < DIM && !err); j++)
			if (matMult_sw[i][j] != matMult_hw[i][j])
				err++;

	// RESULT
	if (err == 0){
		printf("Matrixes identical ... Test successful!\n");

		// SPEEDUP
		printf("SPEEDUP: %f \n", run_time_sw / run_time_hw);
	} else{
		printf("Test failed!\n");
	}

	cleanup_platform();

	return err;
}
