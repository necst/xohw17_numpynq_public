% !TEX root = ../main.tex

\chapter{Design}

In this chapter, a description of the numPYNQ project implementation is provided. We have decided to implement three functions: the Integer Sum, the Dot Product for Integer Matrices and the Full Correlation function. The reasons that led us to choose these functions have been: their widespread use within different Data Science fields; the possibility to compute independently more than one result value, that can result in exploiting hardware parallelism; the capability to demonstrate the applicability of different techniques in integrating functions with the PYNQ platform, such as AXI4-Lite and AXI4-Stream interfaces.

For these purposes, we have used Vivado Design Suite tools by targeting the PYNQ-Z1 board (ZYNQ XC7Z020CLG400-1 part with PYNQ Preset). For each function, the work has been divided into two macro tasks: the Hardware implementation and optimization, and the Software interface creation. Moreover, some of the Software interfaces has been enriched by the optimization of the DMA software routine and Design Reuse was taken into account when designing each overlay. Finally, we have tested each implementation firstly by using Vivado SDK in a standalone application, then by integrating the overlay in the PYNQ platform mounted on the SD card of the PYNQ-Z1 board.

All the designs has been made available (Open Source) for reuse at:\\
\href{https://bitbucket.org/necst/xohw17_numpynq_public}{https://bitbucket.org/necst/xohw17\_numpynq\_public}
\section{Integer Sum}
The first function we have implemented is the Sum between two integer numbers. We have chosen to implement and describe this very simple function both to show the PYNQ overlay creation process, enriching the examples available to the community, and to demonstrate the possibility to use AXI4-Lite control interfaces and the Python \ac{MMIO} library to communicate from the \ac{PS} to the \ac{PL} part of the board.


\subsection{Hardware}
\textit{Vivado HLS}\\
We have created the sum\_hw function that exposes three AXI4-Lite ports: two for the operands and another one to control the IP-Core and read the result of the operation.
\begin{lstlisting}[language=C]
int sum_hw(int a, int b){
#pragma HLS INTERFACE s_axilite port=a
#pragma HLS INTERFACE s_axilite port=b
#pragma HLS INTERFACE s_axilite port=return

	return a + b;
}
\end{lstlisting}

\textit{Vivado}\\
By enabling one of the General Purpose AXI master interface of the ZYNQ7 Processing System we can connect and directly communicate through the AXI4-Lite with the sum\_hw IP-Core.
\begin{figure}[H]
    \centering
    \includegraphics[width=1\textwidth, keepaspectratio=1]{{impl}/{"sum_hw"}.pdf}
    \caption{Block Design of Integer Sum}
\end{figure}


\subsection{Software}
The hardware specification produced by Vivado reports the memory addresses needed to communicate with the AXI4-Lite interface.
\begin{lstlisting}[language=Python]
## Base address of sum_hw accelerator
XSUM_HW_BASE_ADDRESS  =  0x43c00000
XSUM_HW_END_ADDRESS   =  0x43cfffff
XSUM_HW_RANGE         =  XSUM_HW_END_ADDRESS - XSUM_HW_BASE_ADDRESS

## AXILiteS offsets
## 0x00 : Control signals
##        bit 0  - ap_start (Read/Write/SC)
##        bit 1  - ap_done (Read/COR)
##        bit 2  - ap_idle (Read)
##        bit 3  - ap_ready (Read)
##        bit 7  - auto_restart (Read/Write)
##        others - reserved
## 0x10 : Data signal of ap_return
##        bit 31~0 - ap_return[31:0] (Read)
## 0x18 : Data signal of a
##        bit 31~0 - a[31:0] (Read/Write)
## 0x20 : Data signal of b
##        bit 31~0 - b[31:0] (Read/Write)
\end{lstlisting}

The \ac{MMIO} library of Python allows to directly read and write data to a specific memory address. In this way it is possible to control the execution of the IP-Core directly from the user application.
\begin{lstlisting}[language=Python]
from pynq import MMIO
mmio = MMIO(XSUM_HW_BASE_ADDRESS, XSUM_HW_RANGE)
[...]
# set the first addend
self.mmio.write(XSUM_HW_AXILITES_ADDR_A_DATA, a)
# set the second addend
self.mmio.write(XSUM_HW_AXILITES_ADDR_B_DATA, b)
# start the computation
reg_val = self.mmio.read(XSUM_HW_AXILITES_ADDR_AP_CTRL) & 0x80 self.mmio.write(XSUM_HW_AXILITES_ADDR_AP_CTRL, reg_val | 0x01)
# wait done signal
while((self.mmio.read(XSUM_HW_AXILITES_ADDR_AP_CTRL) >> 1) & 0x1):
	pass
# read result
ret_val = self.mmio.read(XSUM_HW_AXILITES_ADDR_AP_RETURN)
\end{lstlisting}


\subsection{Design Reuse}
The implementation of the Integer Sum underlines how it is possible to use simple ports that use the AXI4-Lite light-weight interface to send and receive parameters and control signals between Python applications running on the \ac{PS} and an IP-Core running on the \ac{PL}. The same approach can be used also in complex systems implementations to influence and control the routines executed on the \ac{FPGA}.

\section{Dot Product for Non-fixed Size Matrices}
The second function we have implemented is the Matrix Dot Product. The first design challenge is that we need to take care on how our IP-Core accesses to the input data. In fact, the software implementation that runs on \ac{PS} has the possibility to access each point of the matrices from the host DDR using any stride and type of access, as they are stored as arrays in memory. On the other hands, to achieve low memory access latencies from the \ac{PL}, it is required to use registers (in the form of \acp{LUT}) and \acp{BRAM}, both available in limited quantities. For this reason, it is not possible to store the entire input matrices by only using registers and \acp{BRAM}, unless input matrices have a small number of points. Moreover, we wanted to build an implementation that doesn't have any fixed size constraint.

Thanks to the fact that to compute the (i, j) point of the output matrix it is possible to access sequentially to the points of the i-th row of the first input matrix and of the j-th column of the second input matrix, we have build an IP-Core that exploit two input streams of data to compute each point of the output matrix. On the first input stream, we sequentially send the points of the rows of the first input matrix. Each row is repeated for the number of columns of the second input matrix that are sent sequentially on the second input stream. After reading all points of an entire row of the first input matrix and all points of an entire column of the second input matrix (notice that for matrix dot product those number of points are required to be equal), we can write one point of the output matrix on an output stream. 

Nevertheless, this kind of implementation cannot reach a speedup with respect to software execution by using floating points. This is due to the fact that NumPy uses a highly-optimized, carefully-tuned BLAS method for floating points matrix multiplication, based on the ATLAS project \cite{atlas}. However, we are able to reach a speedup with respect to the unoptimized NumPy implementation of integer numbers matrix multiplication.

\subsection{Hardware}
\textit{Vivado HLS}\\
To create stream interfaces we have exploited the hls\_stream library and we have mapped the ports with AXI4-Stream interface. By using hardware pipelining optimization, it is possible to mask the cost of expensive multiply-and-accumulate operations, and read from the input streams at every clock cycle. Finally, we have let the system to automatically start the IP-Core and we control the execution by sending the desired number of points.
\begin{lstlisting}[language=C]
#include <hls_stream.h>

struct data_struct{
  int data;
  bool last;
};

void matr_mul_stream(hls::stream<int> &x, hls::stream<int> &y, hls::stream<data_struct> &out) {
#pragma HLS INTERFACE axis port=x
#pragma HLS INTERFACE axis port=y
#pragma HLS INTERFACE axis port=out
#pragma HLS INTERFACE ap_ctrl_none port=return

[...]
    for (int i = 1; i < n_iterations * n_points_per_iteration; i++) {
#pragma HLS PIPELINE II=1
        accum = accum + x.read() * y.read();
        [...]
    }
}
\end{lstlisting}

\textit{Vivado}\\
We have inserted two DMAs to send and receive data through the AXI4-Stream interfaces. By enabling one of the AXI high performance slave interface of the ZYNQ7 Processing System we can connect and directly communicate with the DMAs.
\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=1.3\textwidth, keepaspectratio=1]{{impl}/{"mmul_stream"}.pdf}
    }
    \caption{Block Design of Dot Product for Non-fixed Size Matrices}
\end{figure}


\subsection{Software}
Xilinx provides the users with a ready-to-use Python class that manages the DMA operations directly from applications that run on the \ac{PS}. However, we have decided to reengineer the DMA interfaces to reduce the time overhead introduced by the software APIs. We will describe the proposed optimization in a dedicated section within this chapter.

\subsection{Design Reuse}
Exploiting AXI4-Stream interfaces is a commonly used techniques to send huge amount of data from the host \ac{CPU} to the connected \ac{FPGA}. The hardware design of the Dot Product for Non-fixed Size Matrices of Integer Numbers is easily adaptable to different classes of algorithms. In fact, we have used a very similar design, adapted with floating points data streams, in the Correlation function implementation described below.


\section{Dot Product for Fixed Size Matrices}
If we consider small matrices that can be stored by only using registers and \acp{BRAM}, it is possible to significantly reduce the number of input points passed to the IP-Core, by avoiding duplication of the rows and columns, and to better exploit the hardware levels of parallelism to compute results.

\subsection{Hardware}
\textit{Vivado HLS}\\
We have fixed the matrix dimension up to size 84x84. We have exploited partition of the local buffer to allow faster access and better parallel execution. The choice of the local buffers dimension and of the partitioning factor depends on the number of hardware resources available. Finally, the pipeline optimization of the nested loops allows the automatic unroll of the multiply-and-accumulate operations.
\begin{lstlisting}[language=C]
#include <hls_stream.h>

#define DIM 84

struct data_struct{
  float data;
  bool last;
};

void mmult_84(hls::stream<float> &s_in, hls::stream<data_struct> &s_out) {
#pragma HLS INTERFACE axis port=s_in
#pragma HLS INTERFACE axis port=s_out
#pragma HLS INTERFACE ap_ctrl_none port=return

    float a[DIM][DIM];
    float b[DIM][DIM];
    float c[DIM][DIM];

    int const FACTOR = DIM/4;
    #pragma HLS array_partition variable=a block factor=FACTOR dim=2
    #pragma HLS array_partition variable=b block factor=FACTOR dim=1
	[...]
    // matrix multiplication of a A*B matrix
    L1:for (int ia = 0; ia < DIM; ++ia)
        L2:for (int ib = 0; ib < DIM; ++ib)
        {
            #pragma HLS PIPELINE II=1
            float sum = 0;
            L3:for (int id = 0; id < DIM; ++id)
                sum += a[ia][id] * b[id][ib];
            c[ia][ib] = sum;
        }
	[...]
}
\end{lstlisting}

\textit{Vivado}\\
We have inserted one DMA that first send the entire input matrices on the input stream of the IP-Core, then, after execution, receive back the entire output matrix on the output stream of the IP-Core.
\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=1.3\textwidth, keepaspectratio=1]{{impl}/{"mmul_84"}.pdf}
    }
    \caption{Block Design of Dot Product for Fixed Size Matrices}
\end{figure}


\subsection{Software}
Also in this case, we have used the optimized DMA interface to send and receive data between \ac{PS} and \ac{PL} part of the board. One peculiarity is that the design of Dot Product for 84x84 Matrices can be used also with Matrices of smaller dimensions by padding them wit zeros before the hardware execution.

\subsection{Design Reuse}
The design of Dot Product for Fixed Size Matrices underlines how it is possible to exploit the local hardware resources to implement optimization to reduce the execution time and the number of input data to be passed to the IP-Core. This is a technique that can be used both when the input data has a small size and when it is possible to divide the input data into blocks to be saved in the local memory of the FPGA and process them separately each from another. If the board provides more hardware resources this design can be easily adapt and reuse to obtain speedup also for matrices of greater dimension.

\section{Correlation}
The third function we have implemented is the full Correlation. It is a function commonly used in signal processing field and in particular it is used to find similarities or repeating patterns in signals. To obtain one value of the Correlation function it is necessary to shift one of the signals of a given lag and then compute the covariance between the shifted signal and the second one. If we repeat this operation among an interval of lags equal to the size of the signals we obtain the full Correlation function. Our implementation contains different types of optimization, trying to combine the techniques exploited both in the hardware implementation of the Dot Product for Non-fixed Size Matrices and the Dot Product for Fixed Size Matrices. In particular the design takes advantage of hardware parallelism both from the pipeline of the operations on the input streams and by exploiting the presence of local buffers to compute in parallel more than one output values.

\subsection{Hardware}
\textit{Vivado HLS}\\
By using a local buffer that behaves as a shift register, it is possible to compute a number of values of the correlation function in parallel equal to the size of the local buffer; their partial values are stored in the second local buffer, and are written on the output stream only when every points of the signals has been read from the streams. The input streams can be considered divided into blocks: each block has a size equal to the size of the signals. The blocks in the first input stream contains the first signal repeated for an interval of lags and the blocks in the second input stream contains the second signal shifted by the lag contained in the same interval. A padding of zeros, of length equal to the shift, is put in front of each block of the second stream, so that it is possible to read together values from the first and second stream at each clock cycle.

Once again, by employing hardware pipelining and loop unrolling, we were able to mask the latency of multiply-and-accumulate operations. However, as many operations are done in parallel, the execution time of the algorithm is proportional to the input streams size: the local buffers reduce the stream size by a factor equal to local buffers size, which in turn reduces the complexity of the algorithm by the same factor. Being able to use bigger local buffers should lead to even higher performance improvements. Moreover, our implementation can be easily scaled with respect to the available resources on the board, by changing the local buffer size and its partitioning factor.
\begin{lstlisting}[language=C]
#include <hls_stream.h>

struct data_struct{
  float data;
  bool last;
};

int const LOCAL_BUFFER_SIZE = 80;
int const PARTITION_FACTOR = 20;

void correlation(hls::stream<float> &x_in, hls::stream<float> &y_lag_in, hls::stream<data_struct> &correlation_out) {
#pragma HLS INTERFACE axis port=x_in
#pragma HLS INTERFACE axis port=y_lag_in
#pragma HLS INTERFACE axis port=correlation_out
#pragma HLS INTERFACE ap_ctrl_none port=return

	[...]
    // Use a local buffer of size LOCAL_BUFFER_SIZE to calculate the autocorrelation points of one iteration.
    float local_correlation_buffer[LOCAL_BUFFER_SIZE];
#pragma HLS ARRAY_PARTITION variable=local_correlation_buffer block factor=PARTITION_FACTOR dim=1

    // Use a shift register to store a certain amount of points of the signal.
    float shift[LOCAL_BUFFER_SIZE];
#pragma HLS ARRAY_PARTITION variable=shift block factor=PARTITION_FACTOR dim=1
	[...]
	    // ============= PARTIAL CORRELATION COMPUTATION =============
    one_iteration: for (int n = 0; n < num_points; n++) {
    
        // Compute a single iteration, i.e LOCAL_BUFFER_SIZE values of the correlation.
#pragma HLS PIPELINE II=1
        x_i = x_in.read();

        // Shift by 1, and acquire a new point from the signal.
        shift_2: for (int k = LOCAL_BUFFER_SIZE - 1; k > 0; --k) {
#pragma HLS UNROLL
            shift[k] = shift[k - 1];
        }
        shift[0] = y_lag_in.read();

        // ============= PARTIAL SUMS =============
        // Compute the partial sums.
        partial_sums: for (int j = 0; j < LOCAL_BUFFER_SIZE; j++) {
#pragma HLS UNROLL
            local_correlation_buffer[j] += x_i * shift[LAGS_PER_ITERATION - 1 - j];
        }
    }
	[...]
}
\end{lstlisting}

\textit{Vivado}\\
Once again, we have inserted two DMAs to send and receive data through the AXI4-Stream interfaces. By enabling one of the AXI high performance slave interface of the ZYNQ7 Processing System we can connect and directly communicate with the DMAs.
\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=1.3\textwidth, keepaspectratio=1]{{impl}/{"correlate"}.pdf}
    }
    \caption{Block Design of Full Correlation Function}
\end{figure}


\subsection{Software}
Also in this case, we have used the optimized DMA interface to send and receive data between \ac{PS} and \ac{PL} part of the board. The software function also pads the signal before starting hardware execution.

\subsection{Design Reuse}
Exploiting AXI4-Stream interfaces is a commonly used techniques to send huge amount of data from the host \ac{CPU} to the connected \ac{FPGA}. It can be also combined with the exploitation of the local hardware resources to implement optimization to reduce the execution time and the number of input data to be passed to the IP-Core.

\section{DMA Optimization}
To communicate and control DMAs from the \ac{PS}, Xilinx provides the users with a ready-to-use Python class that manages the DMA operations. The reasons that led us to decide to reengineer the DMA interfaces are the ones that follow:
\begin{enumerate}
	\item The Python DMA class, provided by Xilinx, is written using a technique called CFFI. CFFI introduces the possibility to call C functions, included in properly compiled C libraries, directly from Python. Although CFFI is considered a good method to improve the Python application performance, in our context, where we need to exclude as much as possible interfacing overheads, another more light and performing solution had to be found.
	\item The Xilinx DMA module is intended for a general purpose use and for a potential non-expert audience. For this reason, a series of safety control has been introduced in the code. We can avoid allocating time for some of them since we are in charge of writing the interfaces and the custom DMA module that we are going to provide is not intended for non-expert developers.
	\item We needed to approach some of the DMA's routines in other ways. For instance, in the Xilinx DMA class, the initialization of the DMA was coupled with the creation of a local buffer in the board DRAM, while we needed to be able to associate a buffer with a DMA, independently from its initialization.
\end{enumerate}

We chose the Python/C APIs to rewrite our custom DMA module because of their flexibility and expected performance. Python/C API extension module is completely written in C and, thanks to the Python.h library, a dynamic library can be compiled and then imported into a Python application. The Python object included in the Python.h library assures a fully compatible interaction between the C declarations and the Python code while reducing at the minimum all the overheads between Python and C \cite{pythonc-api}. Other solutions, like Cython, could have been exploited for our purpose. Although, none of them were as straightforward as Python/C APIs and the expected effort needed to get comparable performance was much higher.

\begin{figure}[H]
		\centering
		\includegraphics[width=1.125\textwidth]{python-accelerations}
	\caption{Comparison of methods to improve Python performance}
	\label{fig:python-accelerations}
\end{figure}

In order to extract the maximum speedup possible from the data transmission phase of our applications, we have written custom functions to handle the specific data transmission patterns of each of the functions we accelerated. In this way, the pointers both to the buffers that contain the input data and to the one allocated for the operation result are passed to the specific data transmission function and all the rest of the routine is handled by our C compiled DMA module. In particular, we used some of the XAxi interfaces functions that we also used in the Vivado SDK environment to test our IP cores.

Thanks to this work, we have obtained an important speedup in terms of the execution time of our hardware accelerated functions ran by the ARM processor mounted on the board. Moreover, we managed to obtain much more flexibility to handle the local buffers in the lightest way.

\section{Transparency to the user}
Thanks to the PYNQ overlays model, bitstreams can be loaded dynamically by the users directly from their applications and used to configure the FPGA. Moreover, thanks to the Python interface included in the overlay, the user will be able to use the hardware accelerated functions just by calling them like functions of a software library.

From the user point of view, only the Python import will change. If NumPy is imported as follows with Python:

\begin{lstlisting}[language=Python]
import numpy as np
...
c = np.dot(a,b)
...
\end{lstlisting}

\noindent
the numPYNQ overlay would be loaded as follows:

\begin{lstlisting}[language=Python]
import numpynq as np
...
c = np.dot(a,b)
...
\end{lstlisting}

\noindent
The import of the numPYNQ library will allow the user to use the hardware accelerated functions, as long as they are included in the package, in a transparent way and the overlay will program the board with the bitstream needed to execute the task required by the user. If there is not a hardware implementation of the function called by the user, the numPYNQ library automatically delegates the call to the NumPy software implementation of that function.


