Team number: XIL-18638
Project name: numPYNQ
Date: June 30, 2017
Version of uploaded archive: 1.0

University name: Politecnico di Milano

Supervisor name: Marco Domenico Santambrogio
Supervisor e-mail: marco.santambrogio@polimi.it

Participant: Filippo Carloni
Email: filippo.carloni@mail.polimi.it
Participant: Riccardo Pressiani
Email: riccardo.pressiani@mail.polimi.it
Participant: Luca Stornaiuolo
Email: luca.stornaiuolo@mail.polimi.it

Board used: PYNQ-Z1
Vivado Version: Vivado Design Suite 2015.4 / 2016.1
Brief description of project: Nowadays, FPGA-based systems offer a valid solution to accelerate applications in several fields due to the high computational power, the appealing performance per watt and the flexibility offered by these architectures. For instance, scientific computing applications could take advantage of FPGAs technology. However, the non-expert user-base usually cannot exploit the power of these devices because of the deep expertise required. The aim of numPYNQ is to create a hardware library that includes accelerated versions of the core functions of NumPy, a software library for scientific computing commonly used in Python applications. The overlay technology provided by the PYNQ platform allows numPYNQ to extend user applications by programming the FPGA device and offloading part of the computation from the CPU. numPYNQ makes the exploitation process transparent to the end users so that they can take advantage of the high computational power provided by hardware acceleration without approaching the whole FPGA programming learning path.

Description of archive (explain directory structure, documents and source files):
The archive contains three folder:
    - numpynq_release: contains all the stuff necessary to extend the PYNQ platform with the numPYNQ hardware library and the related test benches. Please refer to README file in numpynq_release folder to install and test numPYNQ.
    - hardware_implementation: contains all the source files to build and test functions by using Vivado HLS, Vivado and Vivado SDK. Please refer to README file in hardware_implementation folder to build and test functions.
    - report: contains the report of the project: main.pdf

Instructions to build and test project: please refer to the README files present in numpynq_release folder and in hardware_implementation folder

Link to YouTube Short Video: https://youtu.be/0n4ceHb-Dag
Link to YouTube Playlist: https://www.youtube.com/playlist?list=PLewc2qlpcOufREA9tdnirvpHxWADHOxPD
